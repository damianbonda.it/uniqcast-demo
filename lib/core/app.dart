import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:uniqcast_demo/router/router.dart';
import 'package:uniqcast_demo/theme/color_schemes.g.dart';
import 'package:uniqcast_demo/theme/theme.dart';

class UniqcastApp extends HookConsumerWidget {
  const UniqcastApp({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return MaterialApp.router(
      theme: ThemeData(
          useMaterial3: true,
          colorScheme: lightColorScheme,
          textTheme: textTheme),
      themeMode: ThemeMode.light,
      routerConfig: ref.watch(routerProvider),
    );
  }
}
