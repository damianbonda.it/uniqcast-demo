import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

import '../utils/input_validation.dart';

class CommonInput extends HookWidget {
  const CommonInput(
      {super.key,
      this.autofocus = false,
      this.textInputAction = TextInputAction.next,
      this.validator = _defaultValidator,
      this.callback = _defaultCallback,
      required this.label,
      this.centerText = false,
      this.keyboardType = TextInputType.visiblePassword,
      required this.textEditingController,
      this.obscure = false});

  final String label;
  final TextEditingController textEditingController;
  final bool obscure;
  final bool centerText;
  final TextInputType? keyboardType;
  final TextInputAction textInputAction;
  final String? Function(String) validator;
  final bool autofocus;
  final String? Function(String) callback;

  static String? _defaultValidator(String value) => null;
  static String? _defaultCallback(String value) => null;

  @override
  Widget build(BuildContext context) {
    final isHidden = useState(obscure);

    final ThemeData theme = Theme.of(context);

    final inputBorder = OutlineInputBorder(
        borderRadius: BorderRadius.circular(10),
        borderSide: BorderSide(width: 1));

    return Form(
        autovalidateMode: AutovalidateMode.always,
        child: TextFormField(
            keyboardType: keyboardType,
            onFieldSubmitted: callback,
            autofocus: autofocus,
            textAlign: centerText ? TextAlign.center : TextAlign.start,
            autocorrect: false,
            style: theme.textTheme.bodyLarge
                ?.copyWith(color: theme.colorScheme.primary, fontSize: 16),
            textAlignVertical: TextAlignVertical.center,
            cursorRadius: const Radius.circular(10),
            cursorColor: theme.colorScheme.primary,
            inputFormatters: [
              FilteringTextInputFormatter.deny(
                  InputValidation(context: context).denyEmojisRegex)
            ],
            decoration: InputDecoration(
                alignLabelWithHint: true,
                floatingLabelBehavior: FloatingLabelBehavior.never,
                contentPadding:
                    const EdgeInsets.symmetric(horizontal: 15, vertical: 0),
                hintText: label,
                hintMaxLines: 1,
                hintStyle: theme.textTheme.bodyLarge,
                label: centerText ? Center(child: Text(label)) : Text(label),
                focusedBorder: inputBorder,
                errorMaxLines: 3,
                enabledBorder: inputBorder,
                errorStyle: theme.textTheme.bodySmall
                    ?.copyWith(color: theme.colorScheme.error),
                suffixIcon: obscure
                    ? GestureDetector(
                        onTap: () {
                          isHidden.value = !isHidden.value;
                        },
                        child: Icon(
                          isHidden.value
                              ? Icons.visibility_off_outlined
                              : Icons.visibility_outlined,
                          size: 20,
                          color: theme.colorScheme.primary,
                        ),
                      )
                    : null,
                border: inputBorder),
            textInputAction: textInputAction,
            obscureText: isHidden.value,
            controller: textEditingController,
            validator: (value) => validator(value!)));
  }
}
