import 'package:flutter/material.dart';

class CommonDashboardPage extends StatelessWidget {
  const CommonDashboardPage(
      {super.key,
        required this.appBar,
        required this.content});

  final PreferredSizeWidget appBar;
  final Widget content;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).colorScheme.surface,
      appBar: PreferredSize(preferredSize: const Size.fromHeight(40), child: appBar),
      resizeToAvoidBottomInset: false,
      body: Flex(
        direction: Axis.vertical,
        children: [
          Expanded(
            child: content,
          )
        ],
      ),
    );
  }
}