import 'package:flutter/material.dart';

class CommonFilledButton extends StatelessWidget {
  const CommonFilledButton(
      {super.key,
      required this.action,
      required this.label,
      this.enabled = true,
      this.isLoading = false});

  final VoidCallback action;
  final bool enabled;
  final String label;
  final bool isLoading;

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);

    return Container(
      height: 40,
      width: double.infinity,
      decoration: BoxDecoration(
          color: enabled
              ? theme.primaryColor
              : theme.primaryColor.withOpacity(0.5),
          borderRadius: BorderRadius.circular(10)),
      child: MaterialButton(
        enableFeedback: true,
        onPressed: enabled ? action : null,
        child: isLoading
            ? const SizedBox(
                height: 20,
                width: 20,
                child: CircularProgressIndicator(
                  strokeCap: StrokeCap.round,
                  strokeWidth: 2.0,
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                ),
              )
            : Text(label,
                style: theme.textTheme.bodyMedium
                    ?.copyWith(color: theme.colorScheme.surface)),
      ),
    );
  }
}
