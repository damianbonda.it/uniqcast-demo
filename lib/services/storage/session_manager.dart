import 'package:flutter/cupertino.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:uniqcast_demo/services/storage/secure_storage.dart';
import 'package:uniqcast_demo/services/storage/secure_storage_impl.dart';

class SessionManager extends ChangeNotifier {
  static SessionManager? _instance;
  static const accessTokenKey = "TOKEN";
  static const refreshTokenKey = "REFRESH";
  static const emailKey = "EMAIL";
  static const uidKey = "UID";
  static const oidKey = "OID";

  SecureStorage? _secureStorage;
  String? userEmail;
  String? _access_token;
  String? _refresh_token;
  String? _userId;
  String? _operatorId;
  String? _email;

  bool? _isAuth;

  factory SessionManager({SecureStorage? secureStorage}) {
    if (_instance == null) {
      _instance = SessionManager._internal();
      _instance?._secureStorage = secureStorage ?? SecureStorageImpl();
    }
    return _instance!;
  }

  SessionManager._internal();

  String? get access_token => _access_token;

  String? get refresh_token => _refresh_token;

  String? get userId => _userId;

  bool? get isAuth => _isAuth;

  String? get email => _email;

  String? get operatorId => _operatorId;


  set setAuth(bool value) {
    _isAuth = value;
    notifyListeners();
  }

  Future<void> loadToken() async {
    _access_token = await _secureStorage?.read(key: accessTokenKey);
    _refresh_token = await _secureStorage?.read(key: refreshTokenKey);
    _email = await _secureStorage?.read(key: emailKey);
    _operatorId = await _secureStorage?.read(key: oidKey);
    notifyListeners();
  }

  Future<void> loadUser() async {
    _userId = await _secureStorage?.read(key: uidKey);
  }

  Future<void> saveAccessToken(String token) async {
    await _secureStorage?.write(key: accessTokenKey, value: token);
    _access_token = token;
  }

  Future<void> saveRefreshToken(String token) async {
    await _secureStorage?.write(key: refreshTokenKey, value: token);
    _refresh_token = token;
  }

  Future<void> saveUserId(int uid) async {
    await _secureStorage?.write(key: uidKey, value: uid.toString());
    _userId = uid.toString();
    notifyListeners();
  }

  Future<void> saveOperatorId(String oid) async {
    await _secureStorage?.write(key: oidKey, value: oid);
    _operatorId = oid;
  }

  Future<void> saveEmail(String email) async {
    await _secureStorage?.write(key: emailKey, value: email);
    _email = email;
  }

  Future<void> logout() async {
    _access_token = null;
    _refresh_token = null;
    userEmail = null;
    _email = null;
    _userId = null;
    await _secureStorage?.deleteAll();
    notifyListeners();
  }
}

final sessionManagerProvider = ChangeNotifierProvider<SessionManager>((ref) {
  return SessionManager();
});
