import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:uniqcast_demo/services/storage/secure_storage.dart';

class SecureStorageImpl extends SecureStorage {
  FlutterSecureStorage get _secureStorage => const FlutterSecureStorage();

  @override
  Future<String?> read({required String key}) async =>
      _secureStorage.read(key: key);

  @override
  Future<void> write({required String key, required String value}) async =>
      _secureStorage.write(key: key, value: value);

  @override
  Future<void> deleteAll() async => _secureStorage.deleteAll();

  @override
  Future<void> delete({required String key}) async => _secureStorage.delete(key: key);
}
