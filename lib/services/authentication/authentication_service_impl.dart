import 'dart:async';
import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:uniqcast_demo/constants/app_config.dart';
import 'package:uniqcast_demo/models/error/error_model.dart';
import 'package:uniqcast_demo/models/login/login_model.dart';
import 'package:uniqcast_demo/services/authentication/authentication_service.dart';
import 'package:uniqcast_demo/services/http/api_exception.dart';
import 'package:uniqcast_demo/services/http/api_service.dart';
import 'package:uniqcast_demo/services/storage/session_manager.dart';

class AuthenticationServiceImpl extends AuthenticationService {
  ApiService? _apiService;
  SessionManager? _sessionManager;

  AuthenticationServiceImpl(
      {ApiService? apiService, SessionManager? sessionManager}) {
    _sessionManager = sessionManager ?? SessionManager();
    _apiService = apiService ?? ApiService(sessionManager: _sessionManager!);
  }

  // Log in user with email and password
  @override
  Future<LoginModel> loginUser(String username, String password) async {
    Map<String, String> requestBody = {
      "login_type": "Credentials",
      "username": username,
      "password": password,
      "device":
          "flutter_test_device_${AppConfig.firstName}_${AppConfig.lastName}"
    };

    try {
      Response response =
          await _apiService!.init().post("/v2/global/login", data: requestBody);

      if (response.statusCode == 200) {
        LoginModel _res = LoginModel.fromJson(response.data);

        /// TODO -> Remove, testing purpose
        print(_res.data!.operatorUid);

        _sessionManager?.userEmail = requestBody["username"];
        await _sessionManager?.saveAccessToken(_res.data!.accessToken);
        await _sessionManager?.saveRefreshToken(_res.data!.refreshToken);
        await _sessionManager?.saveOperatorId(_res.data!.operatorUid);
        await _sessionManager?.saveUserId(_res.data!.userId);

        return _res;
      }

      throw ApiException(jsonEncode({
        "statusCode": response.statusCode,
        "severity": "Warning",
        "techMessage": response.data.toString(),
        "userFriendlyMessage": "Something went wrong"
      }));
    } on DioException catch (e) {
      throw ApiException(
          ErrorModel.fromJson(e.response?.data).data.message.toString());
    }
  }

  // Exchange refresh token for new access token
  @override
  Future<String> exchangeRefreshToken() async {
    Map<String, String> requestBody = {
      "refresh_token": _sessionManager!.refresh_token!,
    };

    try {
      Response response = await _apiService!
          .init()
          .post("/v2/global/refresh", data: requestBody);

      if (response.statusCode == 201) {
        LoginModel _res = LoginModel.fromJson(response.data);

        /// TODO -> Remove, testing purpose
        print(_res.data!.refreshToken);

        await _sessionManager?.saveAccessToken(_res.data!.accessToken);
        await _sessionManager?.saveRefreshToken(_res.data!.refreshToken);

        print(_res.data!.refreshToken);

        return _res.data!.accessToken;
      } else {
        await _sessionManager?.logout();
      }

      throw ApiException("Something went wrong");
    } on DioException catch (e) {
      throw ApiException(e.response?.statusMessage ?? "An exception occurred",
          "Api Exception - ");
    }
  }
}
