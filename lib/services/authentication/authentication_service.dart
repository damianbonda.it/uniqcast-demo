import 'package:uniqcast_demo/models/login/login_model.dart';

abstract class AuthenticationService {
  Future<LoginModel> loginUser(String username, String password);
  Future<String> exchangeRefreshToken();
}