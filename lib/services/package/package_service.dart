import 'package:uniqcast_demo/models/package/package_model.dart';

abstract class PackageService {
  Future<PackageModel> getPackages(String deviceClass);
}

enum DeviceClass {
  // Ignore linter
  Mobile
}