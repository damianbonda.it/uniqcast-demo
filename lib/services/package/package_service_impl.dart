import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:uniqcast_demo/models/error/error_model.dart';
import 'package:uniqcast_demo/models/package/package_model.dart';
import 'package:uniqcast_demo/services/http/api_exception.dart';
import 'package:uniqcast_demo/services/http/api_service.dart';
import 'package:uniqcast_demo/services/package/package_service.dart';
import 'package:uniqcast_demo/services/storage/session_manager.dart';

class PackageServiceImpl extends PackageService {
  ApiService? _apiService;
  SessionManager? _sessionManager;

  PackageServiceImpl(
      {ApiService? apiService, SessionManager? sessionManager}) {
    _sessionManager = sessionManager ?? SessionManager();
    _apiService = apiService ?? ApiService();
  }

  @override
  Future<PackageModel> getPackages(String deviceClass) async {

    try {
      Response response = await _apiService!.init().get("/v1/${_sessionManager!.operatorId}/users/${_sessionManager!.userId}/packages?device_class=${deviceClass}");

      if (response.statusCode == 200) {
        PackageModel _res = PackageModel.fromJson(response.data);

        return _res;
      }

      throw ApiException(jsonEncode({
        "statusCode": response.statusCode,
        "severity": "Warning",
        "techMessage": response.data.toString(),
        "userFriendlyMessage": "Something went wrong"
      }));
    } on DioException catch (e) {
      throw ApiException(ErrorModel.fromJson(e.response?.data).data.message.toString());
    }
  }
}