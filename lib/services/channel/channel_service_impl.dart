import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:uniqcast_demo/models/channel/channel_model.dart';
import 'package:uniqcast_demo/models/error/error_model.dart';
import 'package:uniqcast_demo/models/package/package_model.dart';
import 'package:uniqcast_demo/services/channel/channel_service.dart';
import 'package:uniqcast_demo/services/http/api_exception.dart';
import 'package:uniqcast_demo/services/http/api_service.dart';
import 'package:uniqcast_demo/services/package/package_service.dart';
import 'package:uniqcast_demo/services/storage/session_manager.dart';

class ChannelServiceImpl extends ChannelService {
  ApiService? _apiService;
  SessionManager? _sessionManager;

  ChannelServiceImpl(
      {ApiService? apiService, SessionManager? sessionManager}) {
    _sessionManager = sessionManager ?? SessionManager();
    _apiService = apiService ?? ApiService();
  }

  @override
  Future<ChannelModel> getChannels(String package) async {
    try {

      /// TODO -> It should be packages, for example it's being done purely on one package. Can be used as array in conjunction with selectable package.
      Response response = await _apiService!.init().get("/v2/${_sessionManager!.operatorId}/channels?packages=${package}");

      if (response.statusCode == 200) {
        ChannelModel _res = ChannelModel.fromJson(response.data);

        print(_res.toString());

        return _res;
      }

      throw ApiException(jsonEncode({
        "statusCode": response.statusCode,
        "severity": "Warning",
        "techMessage": response.data.toString(),
        "userFriendlyMessage": "Something went wrong"
      }));
    } on DioException catch (e) {
      throw ApiException(ErrorModel.fromJson(e.response?.data).data.message.toString());
    }
  }
}