import 'package:uniqcast_demo/models/channel/channel_model.dart';

abstract class ChannelService {
  Future<ChannelModel> getChannels(String package);
}