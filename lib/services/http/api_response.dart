class ApiResponse<T> {
  T? data;
  Exception? exception;

  ApiResponse.initial();

  ApiResponse.success(this.data);

  ApiResponse.error(this.exception);

  bool get isSuccess {
    return data != null;
  }

  @override
  String toString() {
    if (isSuccess) {
      return '$data';
    }
    return exception.toString();
  }
}
