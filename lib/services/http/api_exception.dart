class ApiException implements Exception {
  final String _message;
  final String _prefix;

  ApiException(this._message, [this._prefix = '']);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ApiException &&
          runtimeType == other.runtimeType &&
          _message == other._message &&
          _prefix == other._prefix;

  @override
  int get hashCode => _message.hashCode ^ _prefix.hashCode;



  @override
  String toString() => "$_prefix$_message";

  String formatted() {
    if (toString().startsWith("Exception: ")) {
      return toString().substring(11);
    }else {
      return toString();
    }
  }
}

class ApiLinkException extends ApiException {
  ApiLinkException(String message) : super(message, "API link exception: ");
}

class AuthenticationException extends ApiException {
  AuthenticationException([String? message])
      : super(message!, "Authentication exception: ");
}