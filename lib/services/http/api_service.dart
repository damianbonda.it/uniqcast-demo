import 'package:dio/dio.dart';
import 'package:uniqcast_demo/constants/app_config.dart';
import 'package:uniqcast_demo/models/login/login_model.dart';
import 'package:uniqcast_demo/services/http/api_exception.dart';
import 'package:uniqcast_demo/services/storage/session_manager.dart';

class ApiService {

  final SessionManager? _sessionManager;

  ApiService({SessionManager? sessionManager}) : _sessionManager = sessionManager ?? SessionManager();

  Dio init() {

    final options = BaseOptions(
      baseUrl: "https://${AppConfig.baseProductionUrl}"
    );

    final dio = Dio(options);

    dio.interceptors.add(
      InterceptorsWrapper(
        onRequest: (options, handler) {
          // Add the access token to the request header
          options.headers["Authorization"] = "Bearer ${_sessionManager?.access_token}";
          return handler.next(options);
        },
        onError: (DioException e, handler) async {
          if (e.response?.statusCode == 401 && _sessionManager?.access_token != null) {

            await _sessionManager?.logout();

            try {
              Map<String, String> requestBody = {
                "refresh_token": _sessionManager!.refresh_token!,
              };

              Response response = await dio.post("/v2/global/refresh", data: requestBody);

              if (response.statusCode == 200) {
                LoginModel _res = LoginModel.fromJson(response.data);

                await _sessionManager?.saveAccessToken(_res.data!.accessToken);
                await _sessionManager?.saveRefreshToken(_res.data!.refreshToken);
              }

              await _sessionManager?.logout();
            } on DioException catch (e) {
              throw ApiException(e.response?.statusMessage ?? "An exception occurred", "Api Exception - ");
            }

            // Update the request header with the new access token
            e.requestOptions.headers["Authorization"] = "Bearer ${_sessionManager?.access_token}";
            // Repeat the request with the updated header
            return handler.resolve(await dio.fetch(e.requestOptions));
          }
          return handler.next(e);
        },
      ),
    );

    return dio;
  }
}
