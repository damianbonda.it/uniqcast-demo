class AppConfig {
  AppConfig._();

  static const String baseProductionUrl = "office-new-dev.uniqcast.com:12611/api/client";

  static const initialRoute = "/dashboard";

  static const loginRoute = "/login";

  static const channelRoute = "channel";

  static const assetsPath = "assets/";

  static const logoAsset = "uniqcast-logo.svg";

  static const firstName = "damian";

  static const lastName = "bonda";

  static const streamUrl = "aHR0cHM6Ly9kZW1vLnVuaWZpZWQtc3RyZWFtaW5nLmNvbS9rOHMvZmVhdHVyZXMvc3RhYmxlL3ZpZGVvL3RlYXJzLW9mLXN0ZWVsL3RlYXJzLW9mLXN0ZWVsLmlzbS8ubTN1OA==";
}
