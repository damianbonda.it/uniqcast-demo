import 'package:flutter/material.dart';
import 'package:uniqcast_demo/common/common_dashboard_page.dart';
import 'package:uniqcast_demo/features/home/home_content.dart';
import 'package:uniqcast_demo/features/home/home_header.dart';
import 'package:uniqcast_demo/features/profile/profile_content.dart';
import 'package:uniqcast_demo/features/profile/profile_header.dart';

class DashboardConfig {
  DashboardConfig._();

  static List<Widget> dashboardPages = [
    const CommonDashboardPage(
      key: PageStorageKey("key_one"),
      appBar: HomeHeader(),
      content: HomeContent(),
    ),
    const CommonDashboardPage(
      key: PageStorageKey("key_two"),
      appBar: ProfileHeader(),
      content: ProfileContent(),
    ),
  ];
}
