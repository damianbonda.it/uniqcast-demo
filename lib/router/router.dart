import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:uniqcast_demo/constants/app_config.dart';
import 'package:uniqcast_demo/features/channel/channel_ui.dart';
import 'package:uniqcast_demo/features/dashboard/dashboard_ui.dart';
import 'package:uniqcast_demo/features/login/login_ui.dart';
import 'package:uniqcast_demo/models/channel/channel_model.dart';
import 'package:uniqcast_demo/services/storage/session_manager.dart';
import 'package:uniqcast_demo/utils/context_service.dart';

final routerProvider = StateProvider<GoRouter>((ref) {
  final userId = ref.watch(sessionManagerProvider.select((value) => value.userId));

  return GoRouter(
      navigatorKey: ContextService.navigatorKey,
      initialLocation: AppConfig.initialRoute,
      routes: [
        GoRoute(
          path: AppConfig.initialRoute,
          builder: (BuildContext context, GoRouterState state) {
            return const DashBoard();
          },
          routes: [
            GoRoute(
              path: AppConfig.channelRoute,
              builder: (BuildContext context, GoRouterState state) {
                String channelId = state.extra as String;
                return Channel(channelId: channelId);
              },
            )
          ]
        ),
        GoRoute(
          path: AppConfig.loginRoute,
          builder: (BuildContext context, GoRouterState state) {
            return const Login();
          },
        )
      ],
      redirect: (context, state) {
        return userId != null ? state.path : AppConfig.loginRoute;
      });
});
