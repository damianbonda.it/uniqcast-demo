import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:flutter_svg/svg.dart';
import 'package:uniqcast_demo/common/common_filled_button.dart';
import 'package:uniqcast_demo/common/common_input.dart';
import 'package:uniqcast_demo/constants/app_config.dart';
import 'package:uniqcast_demo/usecase/login_user_usecase.dart';
import 'package:uniqcast_demo/utils/input_validation.dart';

class Login extends HookConsumerWidget {
  const Login({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final usernameController = useTextEditingController();
    final passwordController = useTextEditingController();

    final usernameValid = useState(false);
    final passValid = useState(false);
    final isLoading = useState(false);

    const String iconPath = "${AppConfig.assetsPath}/${AppConfig.logoAsset}";

    usernameController.addListener(() {
      if (InputValidation(context: context)
                  .nameValidator(usernameController.text) ==
              null &&
          usernameController.text.isNotEmpty) {
        usernameValid.value = true;
      } else {
        usernameValid.value = false;
      }
    });

    passwordController.addListener(() {
      if (passwordController.text.isNotEmpty &&
          passwordController.text.length > 2) {
        passValid.value = true;
      } else {
        passValid.value = false;
      }
    });

    bool isFormValid() {
      if (usernameValid.value && passValid.value) {
        return true;
      } else {
        return false;
      }
    }

    useEffect(() {
      ref.watch(loginUserUseCase).when(initial: () {
        isLoading.value = false;
      }, loading: () {
        isLoading.value = true;
      }, success: (data) async {
        isLoading.value = false;
      }, failure: (error) {
        isLoading.value = false;
        Future.delayed(Duration.zero, () => ScaffoldMessenger.of(context).showSnackBar(SnackBar(showCloseIcon: true, closeIconColor: Theme.of(context).colorScheme.surface, backgroundColor: Theme.of(context).colorScheme.error, content: Text(error.toString(), style: Theme.of(context).textTheme.bodyMedium?.copyWith(color: Theme.of(context).colorScheme.surface)))));
      });
      return;
    }, [ref.watch(loginUserUseCase)]);

    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Theme.of(context).colorScheme.surface,
          body: SafeArea(
            child: AbsorbPointer(
              absorbing: isLoading.value,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Flex(
                  direction: Axis.vertical,
                  children: [
                    const SizedBox(height: 20),
                    AspectRatio(
                      aspectRatio: 4 / 2,
                      child: SvgPicture.asset(iconPath),
                    ),
                    const SizedBox(height: 40),
                    CommonInput(
                        label: "Username",
                        textEditingController: usernameController,
                        validator:
                            InputValidation(context: context).nameValidator),
                    const SizedBox(height: 10),
                    CommonInput(
                        label: "Password",
                        textEditingController: passwordController,
                        obscure: true,
                        callback: (value) {
                          ref.read(loginUserUseCase.notifier).call(
                              usernameController.value.text,
                              passwordController.value.text);
                          return;
                        },
                        textInputAction: TextInputAction.done),
                    const SizedBox(height: 20),
                    CommonFilledButton(
                      enabled: isFormValid(),
                      isLoading: isLoading.value,
                      action: () {
                        ref.read(loginUserUseCase.notifier).call(
                            usernameController.value.text,
                            passwordController.value.text);
                        FocusManager.instance.primaryFocus?.unfocus();
                      },
                      label: "Sign in",
                    ),
                  ],
                ),
              ),
            ),
          )),
    );
  }
}
