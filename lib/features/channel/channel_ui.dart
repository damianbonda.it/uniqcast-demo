import 'dart:convert';
import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:uniqcast_demo/constants/app_config.dart';
import 'package:uniqcast_demo/models/channel/channel_model.dart';
import 'package:uniqcast_demo/usecase/get_channels_usecase.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:video_player/video_player.dart';

class Channel extends HookConsumerWidget {
  const Channel({super.key, required this.channelId});

  final String channelId;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final isLoading = useState(false);
    final channelData = useState(ChannelModel(status: '', data: []));

    useEffect(() {
      WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
        await ref.read(getChannelsUseCase.notifier).call(channelId);
      });
      return;
    }, []);

    String decodedUrl = utf8.decode(base64Url.decode(AppConfig.streamUrl));

    final videoPlayerController = VideoPlayerController.networkUrl(Uri.parse(decodedUrl));

    final chewieController = ChewieController(
      aspectRatio: 16 / 9,
      videoPlayerController: videoPlayerController,
      autoPlay: false,
      looping: true,
    );

    final playerWidget = Chewie(
      controller: chewieController
    );

    ref.watch(getChannelsUseCase).when(initial: () {
      isLoading.value = false;
    }, loading: () {
      isLoading.value = true;
    }, success: (data) async {
      channelData.value = data;
      isLoading.value = false;
    }, failure: (error) {
      isLoading.value = false;
      Future.delayed(
          Duration.zero,
          () => ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              showCloseIcon: true,
              closeIconColor: Theme.of(context).colorScheme.surface,
              backgroundColor: Theme.of(context).colorScheme.error,
              content: Text(error.toString(),
                  style: Theme.of(context).textTheme.bodyMedium?.copyWith(
                      color: Theme.of(context).colorScheme.surface)))));
    });


    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Scaffold(
          appBar: isLoading.value != true ? AppBar(
            title: Text(channelId),
          ) : const PreferredSize(preferredSize: Size.zero, child: SizedBox.shrink()),
          resizeToAvoidBottomInset: false,
          backgroundColor: Theme.of(context).colorScheme.surface,
          body: isLoading.value
              ? const Center(
            child: CircularProgressIndicator(),
          )
              : channelData.value.data!.isEmpty ? const Center(
            child: Text("No channels available"),
          ) : Column(
            children: [
              AspectRatio(
                aspectRatio: 16 / 9,
                child: playerWidget,
              ),
              Expanded(
                flex: 1,
                child: ListView.builder(
                    itemCount: channelData.value.data?.length,
                    itemBuilder: (context, index) {
                      return ListTile(
                        leading: channelData.value.data?[index].logos?.card != null ? SizedBox(
                          width: MediaQuery.of(context).size.width / 3.5,
                          child: Image.network(
                            /// TODO -> Access token left hardcoded on purpose, passing one fetched from API does not work
                            "https://office-new-dev.uniqcast.com:12802/api/client/v1/global/images/${channelData.value.data![index].logos?.card.toString()}?accessKey=WkVjNWNscFhORDBLCg==",
                            color: Colors.green.shade50,
                            errorBuilder: (BuildContext context, Object exception,
                                StackTrace? stackTrace) {
                              return SizedBox(width: MediaQuery.of(context).size.width / 3.5, child: const Text('Error fetching image...'));
                            },
                          ),
                        ) : SizedBox(width: MediaQuery.of(context).size.width / 3.5, child: const Text("No image data available...")),
                        title: Text(channelData.value.data?[index].name ?? ""),
                      );
                    }),
              )
            ],
          )),
    );
  }
}
