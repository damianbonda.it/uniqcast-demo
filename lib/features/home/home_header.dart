import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class HomeHeader extends StatelessWidget implements PreferredSizeWidget {
  const HomeHeader({super.key});

  @override
  Widget build(BuildContext context) {
    return AppBar(
      surfaceTintColor: Colors.transparent,
      centerTitle: false,
      systemOverlayStyle: const SystemUiOverlayStyle(
        statusBarBrightness: Brightness.light,
      ),
      title: Padding(
        padding: EdgeInsets.only(left: 10, top: Platform.isIOS == true ? 0 : 5),
        child: Text(
          "Home",
          style: Theme.of(context).textTheme.headlineMedium,
        ),
      ),
      titleSpacing: 0,
      elevation: 0.0,
      automaticallyImplyLeading: false,
    );
  }

  @override
  Size get preferredSize => throw UnimplementedError();
}