import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:uniqcast_demo/constants/app_config.dart';
import 'package:uniqcast_demo/models/package/package_model.dart';
import 'package:uniqcast_demo/providers/repository_provider.dart';
import 'package:uniqcast_demo/services/package/package_service.dart';
import 'package:uniqcast_demo/usecase/get_channels_usecase.dart';
import 'package:uniqcast_demo/usecase/get_packages_usecase.dart';

class HomeContent extends HookConsumerWidget {
  const HomeContent({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final isLoading = useState(false);
    final isError = useState(false);
    final packageData = useState(PackageModel(status: '', data: []));

    useEffect(() {
      WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
        await ref
            .read(getPackagesUseCase.notifier)
            .call(DeviceClass.Mobile.name);
      });
      return;
    }, []);

    ref.watch(getPackagesUseCase).when(initial: () {
      isLoading.value = false;
    }, loading: () {
      isLoading.value = true;
    }, success: (data) async {
      packageData.value = data;
      isLoading.value = false;
    }, failure: (error) {
      isLoading.value = false;
      isError.value = true;
      Future.delayed(
          Duration.zero,
          () => ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              showCloseIcon: true,
              closeIconColor: Theme.of(context).colorScheme.surface,
              backgroundColor: Theme.of(context).colorScheme.error,
              content: Text(error.toString(),
                  style: Theme.of(context).textTheme.bodyMedium?.copyWith(
                      color: Theme.of(context).colorScheme.surface)))));
    });

    return isLoading.value
        ? const Center(
            child: CircularProgressIndicator(),
          )
        : isError.value ? const Center(
      child: Text("Something went wrong..."),
    ) : GridView.builder(
            shrinkWrap: true,
            padding: const EdgeInsets.symmetric(horizontal: 10),
            itemCount: packageData.value.data?.length,
            itemBuilder: (ctx, i) {
              return GestureDetector(
                onTap: () async {
                  context.go("${AppConfig.initialRoute}/${AppConfig.channelRoute}", extra: packageData.value.data![i].id.toString());
                },
                child: Card(
                  child: Center(
                    child: Text(packageData.value.data![i].id.toString()),
                  ),
                ),
              );
            },
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              childAspectRatio: 1.0,
              crossAxisSpacing: 5,
              mainAxisSpacing: 5,
              mainAxisExtent: 264,
            ),
          );
  }
}
