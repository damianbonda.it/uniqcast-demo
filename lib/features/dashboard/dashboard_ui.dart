import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:uniqcast_demo/constants/dashboard_config.dart';
import 'package:uniqcast_demo/features/dashboard/widget/dashboard_navigation.dart';
import 'package:uniqcast_demo/providers/repository_provider.dart';
import 'package:uniqcast_demo/services/storage/session_manager.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class DashBoard extends HookConsumerWidget {
  const DashBoard({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final navigationState = ref.watch(navigationIndexProvider.state);
    final carouselPages = DashboardConfig.dashboardPages;

    return Scaffold(
        resizeToAvoidBottomInset: false,
        bottomNavigationBar: DashboardNavigation(
            onValueChanged: (int value) => navigationState.state = value,
            updatedIndexValue: navigationState.state
        ),
        extendBody: false,
        body: IndexedStack(index: navigationState.state, children: carouselPages)
        );
  }
}

final navigationIndexProvider = StateProvider.autoDispose<int>((ref) => 0);
