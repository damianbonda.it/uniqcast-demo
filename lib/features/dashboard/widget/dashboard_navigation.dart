import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class DashboardNavigation extends HookWidget {
  const DashboardNavigation(
      {super.key,
      required this.onValueChanged,
      required this.updatedIndexValue});

  final ValueChanged<int> onValueChanged;
  final int updatedIndexValue;

  @override
  Widget build(BuildContext context) {
    final navigationIndex = useState(0);

    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      unselectedItemColor: Theme.of(context).colorScheme.onSurfaceVariant,
      selectedItemColor: Theme.of(context).colorScheme.primary,
      enableFeedback: true,
      showSelectedLabels: false,
      showUnselectedLabels: false,
      iconSize: 24,
      onTap: (index) {
        onValueChanged(index);
        navigationIndex.value = index;
      },
      currentIndex: updatedIndexValue,
      items: const [
        BottomNavigationBarItem(icon: Icon(Icons.home), label: ""),
        BottomNavigationBarItem(icon: Icon(Icons.person), label: ""),
      ],
    );
  }
}
