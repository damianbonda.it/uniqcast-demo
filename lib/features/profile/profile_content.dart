import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:uniqcast_demo/services/storage/session_manager.dart';

class ProfileContent extends HookConsumerWidget {
  const ProfileContent({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Center(
      child: MaterialButton(
        color: Colors.redAccent,
        child: const Text("Log out"),
        onPressed: () {
          ref.read(sessionManagerProvider).logout();
        },
      ),
    );
  }
}
