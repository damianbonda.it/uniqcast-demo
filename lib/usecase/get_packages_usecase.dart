import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:uniqcast_demo/models/login/login_model.dart';
import 'package:uniqcast_demo/models/package/package_model.dart';
import 'package:uniqcast_demo/models/result/result.dart';
import 'package:uniqcast_demo/providers/repository_provider.dart';
import 'package:uniqcast_demo/services/authentication/authentication_service.dart';
import 'package:uniqcast_demo/services/http/api_exception.dart';
import 'package:uniqcast_demo/services/package/package_service.dart';

class GetPackagesUseCase extends StateNotifier<Result<PackageModel>> {
  GetPackagesUseCase(this.packageService) : super(const Result.initial());

  final PackageService packageService;

  Future<void> call(String deviceClass) async {
    state = const Result.loading();

    try {
      final data = await packageService.getPackages(deviceClass);

      state = Result.success(data: data);
    } on Exception catch (e) {
      state = Result.failure(ApiException(e.toString()));
    }
  }
}

final getPackagesUseCase = StateNotifierProvider<GetPackagesUseCase, Result<PackageModel >>((ref) {
  return GetPackagesUseCase(ref.read(packageRepositoryProvider));
});