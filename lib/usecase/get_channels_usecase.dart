import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:uniqcast_demo/models/channel/channel_model.dart';
import 'package:uniqcast_demo/models/login/login_model.dart';
import 'package:uniqcast_demo/models/package/package_model.dart';
import 'package:uniqcast_demo/models/result/result.dart';
import 'package:uniqcast_demo/providers/repository_provider.dart';
import 'package:uniqcast_demo/services/authentication/authentication_service.dart';
import 'package:uniqcast_demo/services/channel/channel_service.dart';
import 'package:uniqcast_demo/services/http/api_exception.dart';
import 'package:uniqcast_demo/services/package/package_service.dart';

class GetChannelsUseCase extends StateNotifier<Result<ChannelModel>> {
  GetChannelsUseCase(this.channelService) : super(const Result.initial());

  final ChannelService channelService;

  Future<void> call(String package) async {
    state = const Result.loading();

    try {
      final data = await channelService.getChannels(package);

      state = Result.success(data: data);
    } on Exception catch (e) {
      state = Result.failure(ApiException(e.toString()));
    }
  }
}

final getChannelsUseCase = StateNotifierProvider<GetChannelsUseCase, Result<ChannelModel >>((ref) {
  return GetChannelsUseCase(ref.read(channelRepositoryProvider));
});