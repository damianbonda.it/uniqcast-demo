import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:uniqcast_demo/models/login/login_model.dart';
import 'package:uniqcast_demo/models/result/result.dart';
import 'package:uniqcast_demo/providers/repository_provider.dart';
import 'package:uniqcast_demo/services/authentication/authentication_service.dart';
import 'package:uniqcast_demo/services/http/api_exception.dart';

class LoginUserUseCase extends StateNotifier<Result<LoginModel>> {
  LoginUserUseCase(this.authenticationService) : super(const Result.initial());

  final AuthenticationService authenticationService;

  Future<void> call(String username, String password) async {
    state = const Result.loading();

    try {
      final data = await authenticationService.loginUser(username, password);

      state = Result.success(data: data);
    } on Exception catch (e) {
      state = Result.failure(ApiException(e.toString()));
    }
  }
}

final loginUserUseCase = StateNotifierProvider<LoginUserUseCase, Result<LoginModel>>((ref) {
  return LoginUserUseCase(ref.read(authenticationRepositoryProvider));
});