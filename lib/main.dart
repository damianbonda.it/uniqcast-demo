import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:is_first_run/is_first_run.dart';
import 'package:uniqcast_demo/core/app.dart';
import 'package:uniqcast_demo/services/storage/session_manager.dart';
import 'package:uniqcast_demo/theme/color_schemes.g.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  SystemChrome.setSystemUIOverlayStyle(
    SystemUiOverlayStyle(
      statusBarColor: lightColorScheme.primary,
    ),
  );

  bool firstRun = await IsFirstRun.isFirstRun();

  firstRun
      ? {
    await SessionManager().logout(),
  }
      : {
    await SessionManager().loadToken(),
    await SessionManager().loadUser(),
  };

  runApp(const ProviderScope(child: UniqcastApp()));
}