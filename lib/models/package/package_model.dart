import 'package:json_annotation/json_annotation.dart';
import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'package_model.freezed.dart';
part 'package_model.g.dart';

@freezed
class Data with _$Data {
  const factory Data({
    @JsonKey(name: 'id') required int id,
    @JsonKey(name: 'purchased') String? purchased,
  }) = _Data;

  factory Data.fromJson(Map<String, dynamic> json) => _$DataFromJson(json);
}

@JsonSerializable()
class PackageModel {
  final String status;
  final List<Data>? data;

  PackageModel({required this.status, required this.data});

  factory PackageModel.fromJson(Map<String, dynamic> json) =>
      _$PackageModelFromJson(json);
  Map<String, dynamic> toJson() => _$PackageModelToJson(this);
}
