import 'package:json_annotation/json_annotation.dart';
import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'login_model.freezed.dart';
part 'login_model.g.dart';

@freezed
class Data with _$Data {
  const factory Data({
    @JsonKey(name: 'token_type') required String tokenType,
    @JsonKey(name: 'access_token') required String accessToken,
    @JsonKey(name: 'refresh_token') required String refreshToken,
    @JsonKey(name: 'expires_in') required String expiresIn,
    @JsonKey(name: 'operator_uid') required String operatorUid,
    @JsonKey(name: 'operator_name') required String operatorName,
    @JsonKey(name: 'user_id') required int userId,
    @JsonKey(name: 'device_id') required int deviceId,
    @JsonKey(name: 'is_multicast_network') required bool isMulticastNetwork,
    @JsonKey(name: 'is_blocked') required bool isBlocked,
    List<String>? subscriberTags,
  }) = _Data;

  factory Data.fromJson(Map<String, dynamic> json) => _$DataFromJson(json);
}

@JsonSerializable()
class LoginModel {
  final String status;
  final Data? data;

  LoginModel({required this.status, required this.data});

  factory LoginModel.fromJson(Map<String, dynamic> json) =>
      _$LoginModelFromJson(json);
  Map<String, dynamic> toJson() => _$LoginModelToJson(this);
}
