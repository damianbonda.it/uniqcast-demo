// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginModel _$LoginModelFromJson(Map<String, dynamic> json) => LoginModel(
      status: json['status'] as String,
      data: json['data'] == null
          ? null
          : Data.fromJson(json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$LoginModelToJson(LoginModel instance) =>
    <String, dynamic>{
      'status': instance.status,
      'data': instance.data,
    };

_$DataImpl _$$DataImplFromJson(Map<String, dynamic> json) => _$DataImpl(
      tokenType: json['token_type'] as String,
      accessToken: json['access_token'] as String,
      refreshToken: json['refresh_token'] as String,
      expiresIn: json['expires_in'] as String,
      operatorUid: json['operator_uid'] as String,
      operatorName: json['operator_name'] as String,
      userId: json['user_id'] as int,
      deviceId: json['device_id'] as int,
      isMulticastNetwork: json['is_multicast_network'] as bool,
      isBlocked: json['is_blocked'] as bool,
      subscriberTags: (json['subscriberTags'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
    );

Map<String, dynamic> _$$DataImplToJson(_$DataImpl instance) =>
    <String, dynamic>{
      'token_type': instance.tokenType,
      'access_token': instance.accessToken,
      'refresh_token': instance.refreshToken,
      'expires_in': instance.expiresIn,
      'operator_uid': instance.operatorUid,
      'operator_name': instance.operatorName,
      'user_id': instance.userId,
      'device_id': instance.deviceId,
      'is_multicast_network': instance.isMulticastNetwork,
      'is_blocked': instance.isBlocked,
      'subscriberTags': instance.subscriberTags,
    };
