// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'login_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

Data _$DataFromJson(Map<String, dynamic> json) {
  return _Data.fromJson(json);
}

/// @nodoc
mixin _$Data {
  @JsonKey(name: 'token_type')
  String get tokenType => throw _privateConstructorUsedError;
  @JsonKey(name: 'access_token')
  String get accessToken => throw _privateConstructorUsedError;
  @JsonKey(name: 'refresh_token')
  String get refreshToken => throw _privateConstructorUsedError;
  @JsonKey(name: 'expires_in')
  String get expiresIn => throw _privateConstructorUsedError;
  @JsonKey(name: 'operator_uid')
  String get operatorUid => throw _privateConstructorUsedError;
  @JsonKey(name: 'operator_name')
  String get operatorName => throw _privateConstructorUsedError;
  @JsonKey(name: 'user_id')
  int get userId => throw _privateConstructorUsedError;
  @JsonKey(name: 'device_id')
  int get deviceId => throw _privateConstructorUsedError;
  @JsonKey(name: 'is_multicast_network')
  bool get isMulticastNetwork => throw _privateConstructorUsedError;
  @JsonKey(name: 'is_blocked')
  bool get isBlocked => throw _privateConstructorUsedError;
  List<String>? get subscriberTags => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $DataCopyWith<Data> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DataCopyWith<$Res> {
  factory $DataCopyWith(Data value, $Res Function(Data) then) =
      _$DataCopyWithImpl<$Res, Data>;
  @useResult
  $Res call(
      {@JsonKey(name: 'token_type') String tokenType,
      @JsonKey(name: 'access_token') String accessToken,
      @JsonKey(name: 'refresh_token') String refreshToken,
      @JsonKey(name: 'expires_in') String expiresIn,
      @JsonKey(name: 'operator_uid') String operatorUid,
      @JsonKey(name: 'operator_name') String operatorName,
      @JsonKey(name: 'user_id') int userId,
      @JsonKey(name: 'device_id') int deviceId,
      @JsonKey(name: 'is_multicast_network') bool isMulticastNetwork,
      @JsonKey(name: 'is_blocked') bool isBlocked,
      List<String>? subscriberTags});
}

/// @nodoc
class _$DataCopyWithImpl<$Res, $Val extends Data>
    implements $DataCopyWith<$Res> {
  _$DataCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? tokenType = null,
    Object? accessToken = null,
    Object? refreshToken = null,
    Object? expiresIn = null,
    Object? operatorUid = null,
    Object? operatorName = null,
    Object? userId = null,
    Object? deviceId = null,
    Object? isMulticastNetwork = null,
    Object? isBlocked = null,
    Object? subscriberTags = freezed,
  }) {
    return _then(_value.copyWith(
      tokenType: null == tokenType
          ? _value.tokenType
          : tokenType // ignore: cast_nullable_to_non_nullable
              as String,
      accessToken: null == accessToken
          ? _value.accessToken
          : accessToken // ignore: cast_nullable_to_non_nullable
              as String,
      refreshToken: null == refreshToken
          ? _value.refreshToken
          : refreshToken // ignore: cast_nullable_to_non_nullable
              as String,
      expiresIn: null == expiresIn
          ? _value.expiresIn
          : expiresIn // ignore: cast_nullable_to_non_nullable
              as String,
      operatorUid: null == operatorUid
          ? _value.operatorUid
          : operatorUid // ignore: cast_nullable_to_non_nullable
              as String,
      operatorName: null == operatorName
          ? _value.operatorName
          : operatorName // ignore: cast_nullable_to_non_nullable
              as String,
      userId: null == userId
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as int,
      deviceId: null == deviceId
          ? _value.deviceId
          : deviceId // ignore: cast_nullable_to_non_nullable
              as int,
      isMulticastNetwork: null == isMulticastNetwork
          ? _value.isMulticastNetwork
          : isMulticastNetwork // ignore: cast_nullable_to_non_nullable
              as bool,
      isBlocked: null == isBlocked
          ? _value.isBlocked
          : isBlocked // ignore: cast_nullable_to_non_nullable
              as bool,
      subscriberTags: freezed == subscriberTags
          ? _value.subscriberTags
          : subscriberTags // ignore: cast_nullable_to_non_nullable
              as List<String>?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$DataImplCopyWith<$Res> implements $DataCopyWith<$Res> {
  factory _$$DataImplCopyWith(
          _$DataImpl value, $Res Function(_$DataImpl) then) =
      __$$DataImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@JsonKey(name: 'token_type') String tokenType,
      @JsonKey(name: 'access_token') String accessToken,
      @JsonKey(name: 'refresh_token') String refreshToken,
      @JsonKey(name: 'expires_in') String expiresIn,
      @JsonKey(name: 'operator_uid') String operatorUid,
      @JsonKey(name: 'operator_name') String operatorName,
      @JsonKey(name: 'user_id') int userId,
      @JsonKey(name: 'device_id') int deviceId,
      @JsonKey(name: 'is_multicast_network') bool isMulticastNetwork,
      @JsonKey(name: 'is_blocked') bool isBlocked,
      List<String>? subscriberTags});
}

/// @nodoc
class __$$DataImplCopyWithImpl<$Res>
    extends _$DataCopyWithImpl<$Res, _$DataImpl>
    implements _$$DataImplCopyWith<$Res> {
  __$$DataImplCopyWithImpl(_$DataImpl _value, $Res Function(_$DataImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? tokenType = null,
    Object? accessToken = null,
    Object? refreshToken = null,
    Object? expiresIn = null,
    Object? operatorUid = null,
    Object? operatorName = null,
    Object? userId = null,
    Object? deviceId = null,
    Object? isMulticastNetwork = null,
    Object? isBlocked = null,
    Object? subscriberTags = freezed,
  }) {
    return _then(_$DataImpl(
      tokenType: null == tokenType
          ? _value.tokenType
          : tokenType // ignore: cast_nullable_to_non_nullable
              as String,
      accessToken: null == accessToken
          ? _value.accessToken
          : accessToken // ignore: cast_nullable_to_non_nullable
              as String,
      refreshToken: null == refreshToken
          ? _value.refreshToken
          : refreshToken // ignore: cast_nullable_to_non_nullable
              as String,
      expiresIn: null == expiresIn
          ? _value.expiresIn
          : expiresIn // ignore: cast_nullable_to_non_nullable
              as String,
      operatorUid: null == operatorUid
          ? _value.operatorUid
          : operatorUid // ignore: cast_nullable_to_non_nullable
              as String,
      operatorName: null == operatorName
          ? _value.operatorName
          : operatorName // ignore: cast_nullable_to_non_nullable
              as String,
      userId: null == userId
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as int,
      deviceId: null == deviceId
          ? _value.deviceId
          : deviceId // ignore: cast_nullable_to_non_nullable
              as int,
      isMulticastNetwork: null == isMulticastNetwork
          ? _value.isMulticastNetwork
          : isMulticastNetwork // ignore: cast_nullable_to_non_nullable
              as bool,
      isBlocked: null == isBlocked
          ? _value.isBlocked
          : isBlocked // ignore: cast_nullable_to_non_nullable
              as bool,
      subscriberTags: freezed == subscriberTags
          ? _value._subscriberTags
          : subscriberTags // ignore: cast_nullable_to_non_nullable
              as List<String>?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$DataImpl with DiagnosticableTreeMixin implements _Data {
  const _$DataImpl(
      {@JsonKey(name: 'token_type') required this.tokenType,
      @JsonKey(name: 'access_token') required this.accessToken,
      @JsonKey(name: 'refresh_token') required this.refreshToken,
      @JsonKey(name: 'expires_in') required this.expiresIn,
      @JsonKey(name: 'operator_uid') required this.operatorUid,
      @JsonKey(name: 'operator_name') required this.operatorName,
      @JsonKey(name: 'user_id') required this.userId,
      @JsonKey(name: 'device_id') required this.deviceId,
      @JsonKey(name: 'is_multicast_network') required this.isMulticastNetwork,
      @JsonKey(name: 'is_blocked') required this.isBlocked,
      final List<String>? subscriberTags})
      : _subscriberTags = subscriberTags;

  factory _$DataImpl.fromJson(Map<String, dynamic> json) =>
      _$$DataImplFromJson(json);

  @override
  @JsonKey(name: 'token_type')
  final String tokenType;
  @override
  @JsonKey(name: 'access_token')
  final String accessToken;
  @override
  @JsonKey(name: 'refresh_token')
  final String refreshToken;
  @override
  @JsonKey(name: 'expires_in')
  final String expiresIn;
  @override
  @JsonKey(name: 'operator_uid')
  final String operatorUid;
  @override
  @JsonKey(name: 'operator_name')
  final String operatorName;
  @override
  @JsonKey(name: 'user_id')
  final int userId;
  @override
  @JsonKey(name: 'device_id')
  final int deviceId;
  @override
  @JsonKey(name: 'is_multicast_network')
  final bool isMulticastNetwork;
  @override
  @JsonKey(name: 'is_blocked')
  final bool isBlocked;
  final List<String>? _subscriberTags;
  @override
  List<String>? get subscriberTags {
    final value = _subscriberTags;
    if (value == null) return null;
    if (_subscriberTags is EqualUnmodifiableListView) return _subscriberTags;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Data(tokenType: $tokenType, accessToken: $accessToken, refreshToken: $refreshToken, expiresIn: $expiresIn, operatorUid: $operatorUid, operatorName: $operatorName, userId: $userId, deviceId: $deviceId, isMulticastNetwork: $isMulticastNetwork, isBlocked: $isBlocked, subscriberTags: $subscriberTags)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'Data'))
      ..add(DiagnosticsProperty('tokenType', tokenType))
      ..add(DiagnosticsProperty('accessToken', accessToken))
      ..add(DiagnosticsProperty('refreshToken', refreshToken))
      ..add(DiagnosticsProperty('expiresIn', expiresIn))
      ..add(DiagnosticsProperty('operatorUid', operatorUid))
      ..add(DiagnosticsProperty('operatorName', operatorName))
      ..add(DiagnosticsProperty('userId', userId))
      ..add(DiagnosticsProperty('deviceId', deviceId))
      ..add(DiagnosticsProperty('isMulticastNetwork', isMulticastNetwork))
      ..add(DiagnosticsProperty('isBlocked', isBlocked))
      ..add(DiagnosticsProperty('subscriberTags', subscriberTags));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$DataImpl &&
            (identical(other.tokenType, tokenType) ||
                other.tokenType == tokenType) &&
            (identical(other.accessToken, accessToken) ||
                other.accessToken == accessToken) &&
            (identical(other.refreshToken, refreshToken) ||
                other.refreshToken == refreshToken) &&
            (identical(other.expiresIn, expiresIn) ||
                other.expiresIn == expiresIn) &&
            (identical(other.operatorUid, operatorUid) ||
                other.operatorUid == operatorUid) &&
            (identical(other.operatorName, operatorName) ||
                other.operatorName == operatorName) &&
            (identical(other.userId, userId) || other.userId == userId) &&
            (identical(other.deviceId, deviceId) ||
                other.deviceId == deviceId) &&
            (identical(other.isMulticastNetwork, isMulticastNetwork) ||
                other.isMulticastNetwork == isMulticastNetwork) &&
            (identical(other.isBlocked, isBlocked) ||
                other.isBlocked == isBlocked) &&
            const DeepCollectionEquality()
                .equals(other._subscriberTags, _subscriberTags));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      tokenType,
      accessToken,
      refreshToken,
      expiresIn,
      operatorUid,
      operatorName,
      userId,
      deviceId,
      isMulticastNetwork,
      isBlocked,
      const DeepCollectionEquality().hash(_subscriberTags));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$DataImplCopyWith<_$DataImpl> get copyWith =>
      __$$DataImplCopyWithImpl<_$DataImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$DataImplToJson(
      this,
    );
  }
}

abstract class _Data implements Data {
  const factory _Data(
      {@JsonKey(name: 'token_type') required final String tokenType,
      @JsonKey(name: 'access_token') required final String accessToken,
      @JsonKey(name: 'refresh_token') required final String refreshToken,
      @JsonKey(name: 'expires_in') required final String expiresIn,
      @JsonKey(name: 'operator_uid') required final String operatorUid,
      @JsonKey(name: 'operator_name') required final String operatorName,
      @JsonKey(name: 'user_id') required final int userId,
      @JsonKey(name: 'device_id') required final int deviceId,
      @JsonKey(name: 'is_multicast_network')
      required final bool isMulticastNetwork,
      @JsonKey(name: 'is_blocked') required final bool isBlocked,
      final List<String>? subscriberTags}) = _$DataImpl;

  factory _Data.fromJson(Map<String, dynamic> json) = _$DataImpl.fromJson;

  @override
  @JsonKey(name: 'token_type')
  String get tokenType;
  @override
  @JsonKey(name: 'access_token')
  String get accessToken;
  @override
  @JsonKey(name: 'refresh_token')
  String get refreshToken;
  @override
  @JsonKey(name: 'expires_in')
  String get expiresIn;
  @override
  @JsonKey(name: 'operator_uid')
  String get operatorUid;
  @override
  @JsonKey(name: 'operator_name')
  String get operatorName;
  @override
  @JsonKey(name: 'user_id')
  int get userId;
  @override
  @JsonKey(name: 'device_id')
  int get deviceId;
  @override
  @JsonKey(name: 'is_multicast_network')
  bool get isMulticastNetwork;
  @override
  @JsonKey(name: 'is_blocked')
  bool get isBlocked;
  @override
  List<String>? get subscriberTags;
  @override
  @JsonKey(ignore: true)
  _$$DataImplCopyWith<_$DataImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
