import 'package:json_annotation/json_annotation.dart';
import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'channel_model.freezed.dart';
part 'channel_model.g.dart';

@freezed
class Data with _$Data {
  const factory Data({
    @JsonKey(name: 'id') required int id,
    @JsonKey(name: 'uid') required String uid,
    @JsonKey(name: 'position') int? position,
    @JsonKey(name: 'type') String? type,
    @JsonKey(name: 'resolution') String? resolution,
    @JsonKey(name: 'is_mcast') bool? isMcast,
    @JsonKey(name: 'is_ott') bool? isOtt,
    @JsonKey(name: 'is_dvbt') bool? isDvbt,
    @JsonKey(name: 'url_mcast') String? urlMcast,
    @JsonKey(name: 'url_ott') String? urlOtt,
    @JsonKey(name: 'recordable') bool? recordable,
    @JsonKey(name: 'rec_duration') int? recDuration,
    @JsonKey(name: 'timeshiftable') bool? timeshiftable,
    @JsonKey(name: 'ts_rec_duration') int? tsRecDuration,
    @JsonKey(name: 'parental_hidden') bool? parentalHidden,
    @JsonKey(name: 'dvbt_tag') String? dvbtTag,
    @JsonKey(name: 'stream_priority') String? streamPriority,
    @JsonKey(name: 'background_image_id') dynamic backgroundImageId,
    @JsonKey(name: 'metadata') String? metadata,
    @JsonKey(name: 'highlights_enabled') bool? highlightsEnabled,
    @JsonKey(name: 'ott_type') String? ottType,
    @JsonKey(name: 'name') String? name,
    @JsonKey(name: 'short_name') String? shortName,
    @JsonKey(name: 'epg_channel') String? epgChannel,
    @JsonKey(name: 'logos') Logos? logos,
    @JsonKey(name: 'mosaic_alignment_values')
    MosaicAlignmentValues? mosaicAlignmentValues,
    @JsonKey(name: 'cm_channel') String? cmChannel,
  }) = _Data;

  factory Data.fromJson(Map<String, dynamic> json) => _$DataFromJson(json);
}

@freezed
class Logos with _$Logos {
  const factory Logos({
    @JsonKey(name: 'CARD') required dynamic card,
    @JsonKey(name: 'LEGACY') required dynamic legacy,
    @JsonKey(name: 'NORMAL') required dynamic normal,
    @JsonKey(name: 'SHADOW') required dynamic shadow,
  }) = _Logos;

  factory Logos.fromJson(Map<String, dynamic> json) => _$LogosFromJson(json);
}

@freezed
class MosaicAlignmentValues with _$MosaicAlignmentValues {
  const factory MosaicAlignmentValues({
    @JsonKey(name: 'gridHeight') dynamic gridHeight,
    @JsonKey(name: 'gridWidth') dynamic gridWidth,
    @JsonKey(name: 'offsetX') dynamic offsetX,
    @JsonKey(name: 'offsetY') dynamic offsetY,
  }) = _MosaicAlignmentValues;

  factory MosaicAlignmentValues.fromJson(Map<String, dynamic> json) =>
      _$MosaicAlignmentValuesFromJson(json);
}

@JsonSerializable()
class ChannelModel {
  final String status;
  final List<Data>? data;

  ChannelModel({required this.status, required this.data});

  factory ChannelModel.fromJson(Map<String, dynamic> json) =>
      _$ChannelModelFromJson(json);
  Map<String, dynamic> toJson() => _$ChannelModelToJson(this);
}
