// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'channel_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

Data _$DataFromJson(Map<String, dynamic> json) {
  return _Data.fromJson(json);
}

/// @nodoc
mixin _$Data {
  @JsonKey(name: 'id')
  int get id => throw _privateConstructorUsedError;
  @JsonKey(name: 'uid')
  String get uid => throw _privateConstructorUsedError;
  @JsonKey(name: 'position')
  int? get position => throw _privateConstructorUsedError;
  @JsonKey(name: 'type')
  String? get type => throw _privateConstructorUsedError;
  @JsonKey(name: 'resolution')
  String? get resolution => throw _privateConstructorUsedError;
  @JsonKey(name: 'is_mcast')
  bool? get isMcast => throw _privateConstructorUsedError;
  @JsonKey(name: 'is_ott')
  bool? get isOtt => throw _privateConstructorUsedError;
  @JsonKey(name: 'is_dvbt')
  bool? get isDvbt => throw _privateConstructorUsedError;
  @JsonKey(name: 'url_mcast')
  String? get urlMcast => throw _privateConstructorUsedError;
  @JsonKey(name: 'url_ott')
  String? get urlOtt => throw _privateConstructorUsedError;
  @JsonKey(name: 'recordable')
  bool? get recordable => throw _privateConstructorUsedError;
  @JsonKey(name: 'rec_duration')
  int? get recDuration => throw _privateConstructorUsedError;
  @JsonKey(name: 'timeshiftable')
  bool? get timeshiftable => throw _privateConstructorUsedError;
  @JsonKey(name: 'ts_rec_duration')
  int? get tsRecDuration => throw _privateConstructorUsedError;
  @JsonKey(name: 'parental_hidden')
  bool? get parentalHidden => throw _privateConstructorUsedError;
  @JsonKey(name: 'dvbt_tag')
  String? get dvbtTag => throw _privateConstructorUsedError;
  @JsonKey(name: 'stream_priority')
  String? get streamPriority => throw _privateConstructorUsedError;
  @JsonKey(name: 'background_image_id')
  dynamic get backgroundImageId => throw _privateConstructorUsedError;
  @JsonKey(name: 'metadata')
  String? get metadata => throw _privateConstructorUsedError;
  @JsonKey(name: 'highlights_enabled')
  bool? get highlightsEnabled => throw _privateConstructorUsedError;
  @JsonKey(name: 'ott_type')
  String? get ottType => throw _privateConstructorUsedError;
  @JsonKey(name: 'name')
  String? get name => throw _privateConstructorUsedError;
  @JsonKey(name: 'short_name')
  String? get shortName => throw _privateConstructorUsedError;
  @JsonKey(name: 'epg_channel')
  String? get epgChannel => throw _privateConstructorUsedError;
  @JsonKey(name: 'logos')
  Logos? get logos => throw _privateConstructorUsedError;
  @JsonKey(name: 'mosaic_alignment_values')
  MosaicAlignmentValues? get mosaicAlignmentValues =>
      throw _privateConstructorUsedError;
  @JsonKey(name: 'cm_channel')
  String? get cmChannel => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $DataCopyWith<Data> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DataCopyWith<$Res> {
  factory $DataCopyWith(Data value, $Res Function(Data) then) =
      _$DataCopyWithImpl<$Res, Data>;
  @useResult
  $Res call(
      {@JsonKey(name: 'id') int id,
      @JsonKey(name: 'uid') String uid,
      @JsonKey(name: 'position') int? position,
      @JsonKey(name: 'type') String? type,
      @JsonKey(name: 'resolution') String? resolution,
      @JsonKey(name: 'is_mcast') bool? isMcast,
      @JsonKey(name: 'is_ott') bool? isOtt,
      @JsonKey(name: 'is_dvbt') bool? isDvbt,
      @JsonKey(name: 'url_mcast') String? urlMcast,
      @JsonKey(name: 'url_ott') String? urlOtt,
      @JsonKey(name: 'recordable') bool? recordable,
      @JsonKey(name: 'rec_duration') int? recDuration,
      @JsonKey(name: 'timeshiftable') bool? timeshiftable,
      @JsonKey(name: 'ts_rec_duration') int? tsRecDuration,
      @JsonKey(name: 'parental_hidden') bool? parentalHidden,
      @JsonKey(name: 'dvbt_tag') String? dvbtTag,
      @JsonKey(name: 'stream_priority') String? streamPriority,
      @JsonKey(name: 'background_image_id') dynamic backgroundImageId,
      @JsonKey(name: 'metadata') String? metadata,
      @JsonKey(name: 'highlights_enabled') bool? highlightsEnabled,
      @JsonKey(name: 'ott_type') String? ottType,
      @JsonKey(name: 'name') String? name,
      @JsonKey(name: 'short_name') String? shortName,
      @JsonKey(name: 'epg_channel') String? epgChannel,
      @JsonKey(name: 'logos') Logos? logos,
      @JsonKey(name: 'mosaic_alignment_values')
      MosaicAlignmentValues? mosaicAlignmentValues,
      @JsonKey(name: 'cm_channel') String? cmChannel});

  $LogosCopyWith<$Res>? get logos;
  $MosaicAlignmentValuesCopyWith<$Res>? get mosaicAlignmentValues;
}

/// @nodoc
class _$DataCopyWithImpl<$Res, $Val extends Data>
    implements $DataCopyWith<$Res> {
  _$DataCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? uid = null,
    Object? position = freezed,
    Object? type = freezed,
    Object? resolution = freezed,
    Object? isMcast = freezed,
    Object? isOtt = freezed,
    Object? isDvbt = freezed,
    Object? urlMcast = freezed,
    Object? urlOtt = freezed,
    Object? recordable = freezed,
    Object? recDuration = freezed,
    Object? timeshiftable = freezed,
    Object? tsRecDuration = freezed,
    Object? parentalHidden = freezed,
    Object? dvbtTag = freezed,
    Object? streamPriority = freezed,
    Object? backgroundImageId = freezed,
    Object? metadata = freezed,
    Object? highlightsEnabled = freezed,
    Object? ottType = freezed,
    Object? name = freezed,
    Object? shortName = freezed,
    Object? epgChannel = freezed,
    Object? logos = freezed,
    Object? mosaicAlignmentValues = freezed,
    Object? cmChannel = freezed,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      uid: null == uid
          ? _value.uid
          : uid // ignore: cast_nullable_to_non_nullable
              as String,
      position: freezed == position
          ? _value.position
          : position // ignore: cast_nullable_to_non_nullable
              as int?,
      type: freezed == type
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as String?,
      resolution: freezed == resolution
          ? _value.resolution
          : resolution // ignore: cast_nullable_to_non_nullable
              as String?,
      isMcast: freezed == isMcast
          ? _value.isMcast
          : isMcast // ignore: cast_nullable_to_non_nullable
              as bool?,
      isOtt: freezed == isOtt
          ? _value.isOtt
          : isOtt // ignore: cast_nullable_to_non_nullable
              as bool?,
      isDvbt: freezed == isDvbt
          ? _value.isDvbt
          : isDvbt // ignore: cast_nullable_to_non_nullable
              as bool?,
      urlMcast: freezed == urlMcast
          ? _value.urlMcast
          : urlMcast // ignore: cast_nullable_to_non_nullable
              as String?,
      urlOtt: freezed == urlOtt
          ? _value.urlOtt
          : urlOtt // ignore: cast_nullable_to_non_nullable
              as String?,
      recordable: freezed == recordable
          ? _value.recordable
          : recordable // ignore: cast_nullable_to_non_nullable
              as bool?,
      recDuration: freezed == recDuration
          ? _value.recDuration
          : recDuration // ignore: cast_nullable_to_non_nullable
              as int?,
      timeshiftable: freezed == timeshiftable
          ? _value.timeshiftable
          : timeshiftable // ignore: cast_nullable_to_non_nullable
              as bool?,
      tsRecDuration: freezed == tsRecDuration
          ? _value.tsRecDuration
          : tsRecDuration // ignore: cast_nullable_to_non_nullable
              as int?,
      parentalHidden: freezed == parentalHidden
          ? _value.parentalHidden
          : parentalHidden // ignore: cast_nullable_to_non_nullable
              as bool?,
      dvbtTag: freezed == dvbtTag
          ? _value.dvbtTag
          : dvbtTag // ignore: cast_nullable_to_non_nullable
              as String?,
      streamPriority: freezed == streamPriority
          ? _value.streamPriority
          : streamPriority // ignore: cast_nullable_to_non_nullable
              as String?,
      backgroundImageId: freezed == backgroundImageId
          ? _value.backgroundImageId
          : backgroundImageId // ignore: cast_nullable_to_non_nullable
              as dynamic,
      metadata: freezed == metadata
          ? _value.metadata
          : metadata // ignore: cast_nullable_to_non_nullable
              as String?,
      highlightsEnabled: freezed == highlightsEnabled
          ? _value.highlightsEnabled
          : highlightsEnabled // ignore: cast_nullable_to_non_nullable
              as bool?,
      ottType: freezed == ottType
          ? _value.ottType
          : ottType // ignore: cast_nullable_to_non_nullable
              as String?,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      shortName: freezed == shortName
          ? _value.shortName
          : shortName // ignore: cast_nullable_to_non_nullable
              as String?,
      epgChannel: freezed == epgChannel
          ? _value.epgChannel
          : epgChannel // ignore: cast_nullable_to_non_nullable
              as String?,
      logos: freezed == logos
          ? _value.logos
          : logos // ignore: cast_nullable_to_non_nullable
              as Logos?,
      mosaicAlignmentValues: freezed == mosaicAlignmentValues
          ? _value.mosaicAlignmentValues
          : mosaicAlignmentValues // ignore: cast_nullable_to_non_nullable
              as MosaicAlignmentValues?,
      cmChannel: freezed == cmChannel
          ? _value.cmChannel
          : cmChannel // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $LogosCopyWith<$Res>? get logos {
    if (_value.logos == null) {
      return null;
    }

    return $LogosCopyWith<$Res>(_value.logos!, (value) {
      return _then(_value.copyWith(logos: value) as $Val);
    });
  }

  @override
  @pragma('vm:prefer-inline')
  $MosaicAlignmentValuesCopyWith<$Res>? get mosaicAlignmentValues {
    if (_value.mosaicAlignmentValues == null) {
      return null;
    }

    return $MosaicAlignmentValuesCopyWith<$Res>(_value.mosaicAlignmentValues!,
        (value) {
      return _then(_value.copyWith(mosaicAlignmentValues: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$DataImplCopyWith<$Res> implements $DataCopyWith<$Res> {
  factory _$$DataImplCopyWith(
          _$DataImpl value, $Res Function(_$DataImpl) then) =
      __$$DataImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@JsonKey(name: 'id') int id,
      @JsonKey(name: 'uid') String uid,
      @JsonKey(name: 'position') int? position,
      @JsonKey(name: 'type') String? type,
      @JsonKey(name: 'resolution') String? resolution,
      @JsonKey(name: 'is_mcast') bool? isMcast,
      @JsonKey(name: 'is_ott') bool? isOtt,
      @JsonKey(name: 'is_dvbt') bool? isDvbt,
      @JsonKey(name: 'url_mcast') String? urlMcast,
      @JsonKey(name: 'url_ott') String? urlOtt,
      @JsonKey(name: 'recordable') bool? recordable,
      @JsonKey(name: 'rec_duration') int? recDuration,
      @JsonKey(name: 'timeshiftable') bool? timeshiftable,
      @JsonKey(name: 'ts_rec_duration') int? tsRecDuration,
      @JsonKey(name: 'parental_hidden') bool? parentalHidden,
      @JsonKey(name: 'dvbt_tag') String? dvbtTag,
      @JsonKey(name: 'stream_priority') String? streamPriority,
      @JsonKey(name: 'background_image_id') dynamic backgroundImageId,
      @JsonKey(name: 'metadata') String? metadata,
      @JsonKey(name: 'highlights_enabled') bool? highlightsEnabled,
      @JsonKey(name: 'ott_type') String? ottType,
      @JsonKey(name: 'name') String? name,
      @JsonKey(name: 'short_name') String? shortName,
      @JsonKey(name: 'epg_channel') String? epgChannel,
      @JsonKey(name: 'logos') Logos? logos,
      @JsonKey(name: 'mosaic_alignment_values')
      MosaicAlignmentValues? mosaicAlignmentValues,
      @JsonKey(name: 'cm_channel') String? cmChannel});

  @override
  $LogosCopyWith<$Res>? get logos;
  @override
  $MosaicAlignmentValuesCopyWith<$Res>? get mosaicAlignmentValues;
}

/// @nodoc
class __$$DataImplCopyWithImpl<$Res>
    extends _$DataCopyWithImpl<$Res, _$DataImpl>
    implements _$$DataImplCopyWith<$Res> {
  __$$DataImplCopyWithImpl(_$DataImpl _value, $Res Function(_$DataImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? uid = null,
    Object? position = freezed,
    Object? type = freezed,
    Object? resolution = freezed,
    Object? isMcast = freezed,
    Object? isOtt = freezed,
    Object? isDvbt = freezed,
    Object? urlMcast = freezed,
    Object? urlOtt = freezed,
    Object? recordable = freezed,
    Object? recDuration = freezed,
    Object? timeshiftable = freezed,
    Object? tsRecDuration = freezed,
    Object? parentalHidden = freezed,
    Object? dvbtTag = freezed,
    Object? streamPriority = freezed,
    Object? backgroundImageId = freezed,
    Object? metadata = freezed,
    Object? highlightsEnabled = freezed,
    Object? ottType = freezed,
    Object? name = freezed,
    Object? shortName = freezed,
    Object? epgChannel = freezed,
    Object? logos = freezed,
    Object? mosaicAlignmentValues = freezed,
    Object? cmChannel = freezed,
  }) {
    return _then(_$DataImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      uid: null == uid
          ? _value.uid
          : uid // ignore: cast_nullable_to_non_nullable
              as String,
      position: freezed == position
          ? _value.position
          : position // ignore: cast_nullable_to_non_nullable
              as int?,
      type: freezed == type
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as String?,
      resolution: freezed == resolution
          ? _value.resolution
          : resolution // ignore: cast_nullable_to_non_nullable
              as String?,
      isMcast: freezed == isMcast
          ? _value.isMcast
          : isMcast // ignore: cast_nullable_to_non_nullable
              as bool?,
      isOtt: freezed == isOtt
          ? _value.isOtt
          : isOtt // ignore: cast_nullable_to_non_nullable
              as bool?,
      isDvbt: freezed == isDvbt
          ? _value.isDvbt
          : isDvbt // ignore: cast_nullable_to_non_nullable
              as bool?,
      urlMcast: freezed == urlMcast
          ? _value.urlMcast
          : urlMcast // ignore: cast_nullable_to_non_nullable
              as String?,
      urlOtt: freezed == urlOtt
          ? _value.urlOtt
          : urlOtt // ignore: cast_nullable_to_non_nullable
              as String?,
      recordable: freezed == recordable
          ? _value.recordable
          : recordable // ignore: cast_nullable_to_non_nullable
              as bool?,
      recDuration: freezed == recDuration
          ? _value.recDuration
          : recDuration // ignore: cast_nullable_to_non_nullable
              as int?,
      timeshiftable: freezed == timeshiftable
          ? _value.timeshiftable
          : timeshiftable // ignore: cast_nullable_to_non_nullable
              as bool?,
      tsRecDuration: freezed == tsRecDuration
          ? _value.tsRecDuration
          : tsRecDuration // ignore: cast_nullable_to_non_nullable
              as int?,
      parentalHidden: freezed == parentalHidden
          ? _value.parentalHidden
          : parentalHidden // ignore: cast_nullable_to_non_nullable
              as bool?,
      dvbtTag: freezed == dvbtTag
          ? _value.dvbtTag
          : dvbtTag // ignore: cast_nullable_to_non_nullable
              as String?,
      streamPriority: freezed == streamPriority
          ? _value.streamPriority
          : streamPriority // ignore: cast_nullable_to_non_nullable
              as String?,
      backgroundImageId: freezed == backgroundImageId
          ? _value.backgroundImageId
          : backgroundImageId // ignore: cast_nullable_to_non_nullable
              as dynamic,
      metadata: freezed == metadata
          ? _value.metadata
          : metadata // ignore: cast_nullable_to_non_nullable
              as String?,
      highlightsEnabled: freezed == highlightsEnabled
          ? _value.highlightsEnabled
          : highlightsEnabled // ignore: cast_nullable_to_non_nullable
              as bool?,
      ottType: freezed == ottType
          ? _value.ottType
          : ottType // ignore: cast_nullable_to_non_nullable
              as String?,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      shortName: freezed == shortName
          ? _value.shortName
          : shortName // ignore: cast_nullable_to_non_nullable
              as String?,
      epgChannel: freezed == epgChannel
          ? _value.epgChannel
          : epgChannel // ignore: cast_nullable_to_non_nullable
              as String?,
      logos: freezed == logos
          ? _value.logos
          : logos // ignore: cast_nullable_to_non_nullable
              as Logos?,
      mosaicAlignmentValues: freezed == mosaicAlignmentValues
          ? _value.mosaicAlignmentValues
          : mosaicAlignmentValues // ignore: cast_nullable_to_non_nullable
              as MosaicAlignmentValues?,
      cmChannel: freezed == cmChannel
          ? _value.cmChannel
          : cmChannel // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$DataImpl with DiagnosticableTreeMixin implements _Data {
  const _$DataImpl(
      {@JsonKey(name: 'id') required this.id,
      @JsonKey(name: 'uid') required this.uid,
      @JsonKey(name: 'position') this.position,
      @JsonKey(name: 'type') this.type,
      @JsonKey(name: 'resolution') this.resolution,
      @JsonKey(name: 'is_mcast') this.isMcast,
      @JsonKey(name: 'is_ott') this.isOtt,
      @JsonKey(name: 'is_dvbt') this.isDvbt,
      @JsonKey(name: 'url_mcast') this.urlMcast,
      @JsonKey(name: 'url_ott') this.urlOtt,
      @JsonKey(name: 'recordable') this.recordable,
      @JsonKey(name: 'rec_duration') this.recDuration,
      @JsonKey(name: 'timeshiftable') this.timeshiftable,
      @JsonKey(name: 'ts_rec_duration') this.tsRecDuration,
      @JsonKey(name: 'parental_hidden') this.parentalHidden,
      @JsonKey(name: 'dvbt_tag') this.dvbtTag,
      @JsonKey(name: 'stream_priority') this.streamPriority,
      @JsonKey(name: 'background_image_id') this.backgroundImageId,
      @JsonKey(name: 'metadata') this.metadata,
      @JsonKey(name: 'highlights_enabled') this.highlightsEnabled,
      @JsonKey(name: 'ott_type') this.ottType,
      @JsonKey(name: 'name') this.name,
      @JsonKey(name: 'short_name') this.shortName,
      @JsonKey(name: 'epg_channel') this.epgChannel,
      @JsonKey(name: 'logos') this.logos,
      @JsonKey(name: 'mosaic_alignment_values') this.mosaicAlignmentValues,
      @JsonKey(name: 'cm_channel') this.cmChannel});

  factory _$DataImpl.fromJson(Map<String, dynamic> json) =>
      _$$DataImplFromJson(json);

  @override
  @JsonKey(name: 'id')
  final int id;
  @override
  @JsonKey(name: 'uid')
  final String uid;
  @override
  @JsonKey(name: 'position')
  final int? position;
  @override
  @JsonKey(name: 'type')
  final String? type;
  @override
  @JsonKey(name: 'resolution')
  final String? resolution;
  @override
  @JsonKey(name: 'is_mcast')
  final bool? isMcast;
  @override
  @JsonKey(name: 'is_ott')
  final bool? isOtt;
  @override
  @JsonKey(name: 'is_dvbt')
  final bool? isDvbt;
  @override
  @JsonKey(name: 'url_mcast')
  final String? urlMcast;
  @override
  @JsonKey(name: 'url_ott')
  final String? urlOtt;
  @override
  @JsonKey(name: 'recordable')
  final bool? recordable;
  @override
  @JsonKey(name: 'rec_duration')
  final int? recDuration;
  @override
  @JsonKey(name: 'timeshiftable')
  final bool? timeshiftable;
  @override
  @JsonKey(name: 'ts_rec_duration')
  final int? tsRecDuration;
  @override
  @JsonKey(name: 'parental_hidden')
  final bool? parentalHidden;
  @override
  @JsonKey(name: 'dvbt_tag')
  final String? dvbtTag;
  @override
  @JsonKey(name: 'stream_priority')
  final String? streamPriority;
  @override
  @JsonKey(name: 'background_image_id')
  final dynamic backgroundImageId;
  @override
  @JsonKey(name: 'metadata')
  final String? metadata;
  @override
  @JsonKey(name: 'highlights_enabled')
  final bool? highlightsEnabled;
  @override
  @JsonKey(name: 'ott_type')
  final String? ottType;
  @override
  @JsonKey(name: 'name')
  final String? name;
  @override
  @JsonKey(name: 'short_name')
  final String? shortName;
  @override
  @JsonKey(name: 'epg_channel')
  final String? epgChannel;
  @override
  @JsonKey(name: 'logos')
  final Logos? logos;
  @override
  @JsonKey(name: 'mosaic_alignment_values')
  final MosaicAlignmentValues? mosaicAlignmentValues;
  @override
  @JsonKey(name: 'cm_channel')
  final String? cmChannel;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Data(id: $id, uid: $uid, position: $position, type: $type, resolution: $resolution, isMcast: $isMcast, isOtt: $isOtt, isDvbt: $isDvbt, urlMcast: $urlMcast, urlOtt: $urlOtt, recordable: $recordable, recDuration: $recDuration, timeshiftable: $timeshiftable, tsRecDuration: $tsRecDuration, parentalHidden: $parentalHidden, dvbtTag: $dvbtTag, streamPriority: $streamPriority, backgroundImageId: $backgroundImageId, metadata: $metadata, highlightsEnabled: $highlightsEnabled, ottType: $ottType, name: $name, shortName: $shortName, epgChannel: $epgChannel, logos: $logos, mosaicAlignmentValues: $mosaicAlignmentValues, cmChannel: $cmChannel)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'Data'))
      ..add(DiagnosticsProperty('id', id))
      ..add(DiagnosticsProperty('uid', uid))
      ..add(DiagnosticsProperty('position', position))
      ..add(DiagnosticsProperty('type', type))
      ..add(DiagnosticsProperty('resolution', resolution))
      ..add(DiagnosticsProperty('isMcast', isMcast))
      ..add(DiagnosticsProperty('isOtt', isOtt))
      ..add(DiagnosticsProperty('isDvbt', isDvbt))
      ..add(DiagnosticsProperty('urlMcast', urlMcast))
      ..add(DiagnosticsProperty('urlOtt', urlOtt))
      ..add(DiagnosticsProperty('recordable', recordable))
      ..add(DiagnosticsProperty('recDuration', recDuration))
      ..add(DiagnosticsProperty('timeshiftable', timeshiftable))
      ..add(DiagnosticsProperty('tsRecDuration', tsRecDuration))
      ..add(DiagnosticsProperty('parentalHidden', parentalHidden))
      ..add(DiagnosticsProperty('dvbtTag', dvbtTag))
      ..add(DiagnosticsProperty('streamPriority', streamPriority))
      ..add(DiagnosticsProperty('backgroundImageId', backgroundImageId))
      ..add(DiagnosticsProperty('metadata', metadata))
      ..add(DiagnosticsProperty('highlightsEnabled', highlightsEnabled))
      ..add(DiagnosticsProperty('ottType', ottType))
      ..add(DiagnosticsProperty('name', name))
      ..add(DiagnosticsProperty('shortName', shortName))
      ..add(DiagnosticsProperty('epgChannel', epgChannel))
      ..add(DiagnosticsProperty('logos', logos))
      ..add(DiagnosticsProperty('mosaicAlignmentValues', mosaicAlignmentValues))
      ..add(DiagnosticsProperty('cmChannel', cmChannel));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$DataImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.uid, uid) || other.uid == uid) &&
            (identical(other.position, position) ||
                other.position == position) &&
            (identical(other.type, type) || other.type == type) &&
            (identical(other.resolution, resolution) ||
                other.resolution == resolution) &&
            (identical(other.isMcast, isMcast) || other.isMcast == isMcast) &&
            (identical(other.isOtt, isOtt) || other.isOtt == isOtt) &&
            (identical(other.isDvbt, isDvbt) || other.isDvbt == isDvbt) &&
            (identical(other.urlMcast, urlMcast) ||
                other.urlMcast == urlMcast) &&
            (identical(other.urlOtt, urlOtt) || other.urlOtt == urlOtt) &&
            (identical(other.recordable, recordable) ||
                other.recordable == recordable) &&
            (identical(other.recDuration, recDuration) ||
                other.recDuration == recDuration) &&
            (identical(other.timeshiftable, timeshiftable) ||
                other.timeshiftable == timeshiftable) &&
            (identical(other.tsRecDuration, tsRecDuration) ||
                other.tsRecDuration == tsRecDuration) &&
            (identical(other.parentalHidden, parentalHidden) ||
                other.parentalHidden == parentalHidden) &&
            (identical(other.dvbtTag, dvbtTag) || other.dvbtTag == dvbtTag) &&
            (identical(other.streamPriority, streamPriority) ||
                other.streamPriority == streamPriority) &&
            const DeepCollectionEquality()
                .equals(other.backgroundImageId, backgroundImageId) &&
            (identical(other.metadata, metadata) ||
                other.metadata == metadata) &&
            (identical(other.highlightsEnabled, highlightsEnabled) ||
                other.highlightsEnabled == highlightsEnabled) &&
            (identical(other.ottType, ottType) || other.ottType == ottType) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.shortName, shortName) ||
                other.shortName == shortName) &&
            (identical(other.epgChannel, epgChannel) ||
                other.epgChannel == epgChannel) &&
            (identical(other.logos, logos) || other.logos == logos) &&
            (identical(other.mosaicAlignmentValues, mosaicAlignmentValues) ||
                other.mosaicAlignmentValues == mosaicAlignmentValues) &&
            (identical(other.cmChannel, cmChannel) ||
                other.cmChannel == cmChannel));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        id,
        uid,
        position,
        type,
        resolution,
        isMcast,
        isOtt,
        isDvbt,
        urlMcast,
        urlOtt,
        recordable,
        recDuration,
        timeshiftable,
        tsRecDuration,
        parentalHidden,
        dvbtTag,
        streamPriority,
        const DeepCollectionEquality().hash(backgroundImageId),
        metadata,
        highlightsEnabled,
        ottType,
        name,
        shortName,
        epgChannel,
        logos,
        mosaicAlignmentValues,
        cmChannel
      ]);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$DataImplCopyWith<_$DataImpl> get copyWith =>
      __$$DataImplCopyWithImpl<_$DataImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$DataImplToJson(
      this,
    );
  }
}

abstract class _Data implements Data {
  const factory _Data(
      {@JsonKey(name: 'id') required final int id,
      @JsonKey(name: 'uid') required final String uid,
      @JsonKey(name: 'position') final int? position,
      @JsonKey(name: 'type') final String? type,
      @JsonKey(name: 'resolution') final String? resolution,
      @JsonKey(name: 'is_mcast') final bool? isMcast,
      @JsonKey(name: 'is_ott') final bool? isOtt,
      @JsonKey(name: 'is_dvbt') final bool? isDvbt,
      @JsonKey(name: 'url_mcast') final String? urlMcast,
      @JsonKey(name: 'url_ott') final String? urlOtt,
      @JsonKey(name: 'recordable') final bool? recordable,
      @JsonKey(name: 'rec_duration') final int? recDuration,
      @JsonKey(name: 'timeshiftable') final bool? timeshiftable,
      @JsonKey(name: 'ts_rec_duration') final int? tsRecDuration,
      @JsonKey(name: 'parental_hidden') final bool? parentalHidden,
      @JsonKey(name: 'dvbt_tag') final String? dvbtTag,
      @JsonKey(name: 'stream_priority') final String? streamPriority,
      @JsonKey(name: 'background_image_id') final dynamic backgroundImageId,
      @JsonKey(name: 'metadata') final String? metadata,
      @JsonKey(name: 'highlights_enabled') final bool? highlightsEnabled,
      @JsonKey(name: 'ott_type') final String? ottType,
      @JsonKey(name: 'name') final String? name,
      @JsonKey(name: 'short_name') final String? shortName,
      @JsonKey(name: 'epg_channel') final String? epgChannel,
      @JsonKey(name: 'logos') final Logos? logos,
      @JsonKey(name: 'mosaic_alignment_values')
      final MosaicAlignmentValues? mosaicAlignmentValues,
      @JsonKey(name: 'cm_channel') final String? cmChannel}) = _$DataImpl;

  factory _Data.fromJson(Map<String, dynamic> json) = _$DataImpl.fromJson;

  @override
  @JsonKey(name: 'id')
  int get id;
  @override
  @JsonKey(name: 'uid')
  String get uid;
  @override
  @JsonKey(name: 'position')
  int? get position;
  @override
  @JsonKey(name: 'type')
  String? get type;
  @override
  @JsonKey(name: 'resolution')
  String? get resolution;
  @override
  @JsonKey(name: 'is_mcast')
  bool? get isMcast;
  @override
  @JsonKey(name: 'is_ott')
  bool? get isOtt;
  @override
  @JsonKey(name: 'is_dvbt')
  bool? get isDvbt;
  @override
  @JsonKey(name: 'url_mcast')
  String? get urlMcast;
  @override
  @JsonKey(name: 'url_ott')
  String? get urlOtt;
  @override
  @JsonKey(name: 'recordable')
  bool? get recordable;
  @override
  @JsonKey(name: 'rec_duration')
  int? get recDuration;
  @override
  @JsonKey(name: 'timeshiftable')
  bool? get timeshiftable;
  @override
  @JsonKey(name: 'ts_rec_duration')
  int? get tsRecDuration;
  @override
  @JsonKey(name: 'parental_hidden')
  bool? get parentalHidden;
  @override
  @JsonKey(name: 'dvbt_tag')
  String? get dvbtTag;
  @override
  @JsonKey(name: 'stream_priority')
  String? get streamPriority;
  @override
  @JsonKey(name: 'background_image_id')
  dynamic get backgroundImageId;
  @override
  @JsonKey(name: 'metadata')
  String? get metadata;
  @override
  @JsonKey(name: 'highlights_enabled')
  bool? get highlightsEnabled;
  @override
  @JsonKey(name: 'ott_type')
  String? get ottType;
  @override
  @JsonKey(name: 'name')
  String? get name;
  @override
  @JsonKey(name: 'short_name')
  String? get shortName;
  @override
  @JsonKey(name: 'epg_channel')
  String? get epgChannel;
  @override
  @JsonKey(name: 'logos')
  Logos? get logos;
  @override
  @JsonKey(name: 'mosaic_alignment_values')
  MosaicAlignmentValues? get mosaicAlignmentValues;
  @override
  @JsonKey(name: 'cm_channel')
  String? get cmChannel;
  @override
  @JsonKey(ignore: true)
  _$$DataImplCopyWith<_$DataImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

Logos _$LogosFromJson(Map<String, dynamic> json) {
  return _Logos.fromJson(json);
}

/// @nodoc
mixin _$Logos {
  @JsonKey(name: 'CARD')
  dynamic get card => throw _privateConstructorUsedError;
  @JsonKey(name: 'LEGACY')
  dynamic get legacy => throw _privateConstructorUsedError;
  @JsonKey(name: 'NORMAL')
  dynamic get normal => throw _privateConstructorUsedError;
  @JsonKey(name: 'SHADOW')
  dynamic get shadow => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $LogosCopyWith<Logos> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LogosCopyWith<$Res> {
  factory $LogosCopyWith(Logos value, $Res Function(Logos) then) =
      _$LogosCopyWithImpl<$Res, Logos>;
  @useResult
  $Res call(
      {@JsonKey(name: 'CARD') dynamic card,
      @JsonKey(name: 'LEGACY') dynamic legacy,
      @JsonKey(name: 'NORMAL') dynamic normal,
      @JsonKey(name: 'SHADOW') dynamic shadow});
}

/// @nodoc
class _$LogosCopyWithImpl<$Res, $Val extends Logos>
    implements $LogosCopyWith<$Res> {
  _$LogosCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? card = freezed,
    Object? legacy = freezed,
    Object? normal = freezed,
    Object? shadow = freezed,
  }) {
    return _then(_value.copyWith(
      card: freezed == card
          ? _value.card
          : card // ignore: cast_nullable_to_non_nullable
              as dynamic,
      legacy: freezed == legacy
          ? _value.legacy
          : legacy // ignore: cast_nullable_to_non_nullable
              as dynamic,
      normal: freezed == normal
          ? _value.normal
          : normal // ignore: cast_nullable_to_non_nullable
              as dynamic,
      shadow: freezed == shadow
          ? _value.shadow
          : shadow // ignore: cast_nullable_to_non_nullable
              as dynamic,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$LogosImplCopyWith<$Res> implements $LogosCopyWith<$Res> {
  factory _$$LogosImplCopyWith(
          _$LogosImpl value, $Res Function(_$LogosImpl) then) =
      __$$LogosImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@JsonKey(name: 'CARD') dynamic card,
      @JsonKey(name: 'LEGACY') dynamic legacy,
      @JsonKey(name: 'NORMAL') dynamic normal,
      @JsonKey(name: 'SHADOW') dynamic shadow});
}

/// @nodoc
class __$$LogosImplCopyWithImpl<$Res>
    extends _$LogosCopyWithImpl<$Res, _$LogosImpl>
    implements _$$LogosImplCopyWith<$Res> {
  __$$LogosImplCopyWithImpl(
      _$LogosImpl _value, $Res Function(_$LogosImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? card = freezed,
    Object? legacy = freezed,
    Object? normal = freezed,
    Object? shadow = freezed,
  }) {
    return _then(_$LogosImpl(
      card: freezed == card
          ? _value.card
          : card // ignore: cast_nullable_to_non_nullable
              as dynamic,
      legacy: freezed == legacy
          ? _value.legacy
          : legacy // ignore: cast_nullable_to_non_nullable
              as dynamic,
      normal: freezed == normal
          ? _value.normal
          : normal // ignore: cast_nullable_to_non_nullable
              as dynamic,
      shadow: freezed == shadow
          ? _value.shadow
          : shadow // ignore: cast_nullable_to_non_nullable
              as dynamic,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$LogosImpl with DiagnosticableTreeMixin implements _Logos {
  const _$LogosImpl(
      {@JsonKey(name: 'CARD') required this.card,
      @JsonKey(name: 'LEGACY') required this.legacy,
      @JsonKey(name: 'NORMAL') required this.normal,
      @JsonKey(name: 'SHADOW') required this.shadow});

  factory _$LogosImpl.fromJson(Map<String, dynamic> json) =>
      _$$LogosImplFromJson(json);

  @override
  @JsonKey(name: 'CARD')
  final dynamic card;
  @override
  @JsonKey(name: 'LEGACY')
  final dynamic legacy;
  @override
  @JsonKey(name: 'NORMAL')
  final dynamic normal;
  @override
  @JsonKey(name: 'SHADOW')
  final dynamic shadow;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Logos(card: $card, legacy: $legacy, normal: $normal, shadow: $shadow)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'Logos'))
      ..add(DiagnosticsProperty('card', card))
      ..add(DiagnosticsProperty('legacy', legacy))
      ..add(DiagnosticsProperty('normal', normal))
      ..add(DiagnosticsProperty('shadow', shadow));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LogosImpl &&
            const DeepCollectionEquality().equals(other.card, card) &&
            const DeepCollectionEquality().equals(other.legacy, legacy) &&
            const DeepCollectionEquality().equals(other.normal, normal) &&
            const DeepCollectionEquality().equals(other.shadow, shadow));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(card),
      const DeepCollectionEquality().hash(legacy),
      const DeepCollectionEquality().hash(normal),
      const DeepCollectionEquality().hash(shadow));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$LogosImplCopyWith<_$LogosImpl> get copyWith =>
      __$$LogosImplCopyWithImpl<_$LogosImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$LogosImplToJson(
      this,
    );
  }
}

abstract class _Logos implements Logos {
  const factory _Logos(
      {@JsonKey(name: 'CARD') required final dynamic card,
      @JsonKey(name: 'LEGACY') required final dynamic legacy,
      @JsonKey(name: 'NORMAL') required final dynamic normal,
      @JsonKey(name: 'SHADOW') required final dynamic shadow}) = _$LogosImpl;

  factory _Logos.fromJson(Map<String, dynamic> json) = _$LogosImpl.fromJson;

  @override
  @JsonKey(name: 'CARD')
  dynamic get card;
  @override
  @JsonKey(name: 'LEGACY')
  dynamic get legacy;
  @override
  @JsonKey(name: 'NORMAL')
  dynamic get normal;
  @override
  @JsonKey(name: 'SHADOW')
  dynamic get shadow;
  @override
  @JsonKey(ignore: true)
  _$$LogosImplCopyWith<_$LogosImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

MosaicAlignmentValues _$MosaicAlignmentValuesFromJson(
    Map<String, dynamic> json) {
  return _MosaicAlignmentValues.fromJson(json);
}

/// @nodoc
mixin _$MosaicAlignmentValues {
  @JsonKey(name: 'gridHeight')
  dynamic get gridHeight => throw _privateConstructorUsedError;
  @JsonKey(name: 'gridWidth')
  dynamic get gridWidth => throw _privateConstructorUsedError;
  @JsonKey(name: 'offsetX')
  dynamic get offsetX => throw _privateConstructorUsedError;
  @JsonKey(name: 'offsetY')
  dynamic get offsetY => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $MosaicAlignmentValuesCopyWith<MosaicAlignmentValues> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MosaicAlignmentValuesCopyWith<$Res> {
  factory $MosaicAlignmentValuesCopyWith(MosaicAlignmentValues value,
          $Res Function(MosaicAlignmentValues) then) =
      _$MosaicAlignmentValuesCopyWithImpl<$Res, MosaicAlignmentValues>;
  @useResult
  $Res call(
      {@JsonKey(name: 'gridHeight') dynamic gridHeight,
      @JsonKey(name: 'gridWidth') dynamic gridWidth,
      @JsonKey(name: 'offsetX') dynamic offsetX,
      @JsonKey(name: 'offsetY') dynamic offsetY});
}

/// @nodoc
class _$MosaicAlignmentValuesCopyWithImpl<$Res,
        $Val extends MosaicAlignmentValues>
    implements $MosaicAlignmentValuesCopyWith<$Res> {
  _$MosaicAlignmentValuesCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? gridHeight = freezed,
    Object? gridWidth = freezed,
    Object? offsetX = freezed,
    Object? offsetY = freezed,
  }) {
    return _then(_value.copyWith(
      gridHeight: freezed == gridHeight
          ? _value.gridHeight
          : gridHeight // ignore: cast_nullable_to_non_nullable
              as dynamic,
      gridWidth: freezed == gridWidth
          ? _value.gridWidth
          : gridWidth // ignore: cast_nullable_to_non_nullable
              as dynamic,
      offsetX: freezed == offsetX
          ? _value.offsetX
          : offsetX // ignore: cast_nullable_to_non_nullable
              as dynamic,
      offsetY: freezed == offsetY
          ? _value.offsetY
          : offsetY // ignore: cast_nullable_to_non_nullable
              as dynamic,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$MosaicAlignmentValuesImplCopyWith<$Res>
    implements $MosaicAlignmentValuesCopyWith<$Res> {
  factory _$$MosaicAlignmentValuesImplCopyWith(
          _$MosaicAlignmentValuesImpl value,
          $Res Function(_$MosaicAlignmentValuesImpl) then) =
      __$$MosaicAlignmentValuesImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@JsonKey(name: 'gridHeight') dynamic gridHeight,
      @JsonKey(name: 'gridWidth') dynamic gridWidth,
      @JsonKey(name: 'offsetX') dynamic offsetX,
      @JsonKey(name: 'offsetY') dynamic offsetY});
}

/// @nodoc
class __$$MosaicAlignmentValuesImplCopyWithImpl<$Res>
    extends _$MosaicAlignmentValuesCopyWithImpl<$Res,
        _$MosaicAlignmentValuesImpl>
    implements _$$MosaicAlignmentValuesImplCopyWith<$Res> {
  __$$MosaicAlignmentValuesImplCopyWithImpl(_$MosaicAlignmentValuesImpl _value,
      $Res Function(_$MosaicAlignmentValuesImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? gridHeight = freezed,
    Object? gridWidth = freezed,
    Object? offsetX = freezed,
    Object? offsetY = freezed,
  }) {
    return _then(_$MosaicAlignmentValuesImpl(
      gridHeight: freezed == gridHeight
          ? _value.gridHeight
          : gridHeight // ignore: cast_nullable_to_non_nullable
              as dynamic,
      gridWidth: freezed == gridWidth
          ? _value.gridWidth
          : gridWidth // ignore: cast_nullable_to_non_nullable
              as dynamic,
      offsetX: freezed == offsetX
          ? _value.offsetX
          : offsetX // ignore: cast_nullable_to_non_nullable
              as dynamic,
      offsetY: freezed == offsetY
          ? _value.offsetY
          : offsetY // ignore: cast_nullable_to_non_nullable
              as dynamic,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$MosaicAlignmentValuesImpl
    with DiagnosticableTreeMixin
    implements _MosaicAlignmentValues {
  const _$MosaicAlignmentValuesImpl(
      {@JsonKey(name: 'gridHeight') this.gridHeight,
      @JsonKey(name: 'gridWidth') this.gridWidth,
      @JsonKey(name: 'offsetX') this.offsetX,
      @JsonKey(name: 'offsetY') this.offsetY});

  factory _$MosaicAlignmentValuesImpl.fromJson(Map<String, dynamic> json) =>
      _$$MosaicAlignmentValuesImplFromJson(json);

  @override
  @JsonKey(name: 'gridHeight')
  final dynamic gridHeight;
  @override
  @JsonKey(name: 'gridWidth')
  final dynamic gridWidth;
  @override
  @JsonKey(name: 'offsetX')
  final dynamic offsetX;
  @override
  @JsonKey(name: 'offsetY')
  final dynamic offsetY;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'MosaicAlignmentValues(gridHeight: $gridHeight, gridWidth: $gridWidth, offsetX: $offsetX, offsetY: $offsetY)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'MosaicAlignmentValues'))
      ..add(DiagnosticsProperty('gridHeight', gridHeight))
      ..add(DiagnosticsProperty('gridWidth', gridWidth))
      ..add(DiagnosticsProperty('offsetX', offsetX))
      ..add(DiagnosticsProperty('offsetY', offsetY));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$MosaicAlignmentValuesImpl &&
            const DeepCollectionEquality()
                .equals(other.gridHeight, gridHeight) &&
            const DeepCollectionEquality().equals(other.gridWidth, gridWidth) &&
            const DeepCollectionEquality().equals(other.offsetX, offsetX) &&
            const DeepCollectionEquality().equals(other.offsetY, offsetY));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(gridHeight),
      const DeepCollectionEquality().hash(gridWidth),
      const DeepCollectionEquality().hash(offsetX),
      const DeepCollectionEquality().hash(offsetY));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$MosaicAlignmentValuesImplCopyWith<_$MosaicAlignmentValuesImpl>
      get copyWith => __$$MosaicAlignmentValuesImplCopyWithImpl<
          _$MosaicAlignmentValuesImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$MosaicAlignmentValuesImplToJson(
      this,
    );
  }
}

abstract class _MosaicAlignmentValues implements MosaicAlignmentValues {
  const factory _MosaicAlignmentValues(
          {@JsonKey(name: 'gridHeight') final dynamic gridHeight,
          @JsonKey(name: 'gridWidth') final dynamic gridWidth,
          @JsonKey(name: 'offsetX') final dynamic offsetX,
          @JsonKey(name: 'offsetY') final dynamic offsetY}) =
      _$MosaicAlignmentValuesImpl;

  factory _MosaicAlignmentValues.fromJson(Map<String, dynamic> json) =
      _$MosaicAlignmentValuesImpl.fromJson;

  @override
  @JsonKey(name: 'gridHeight')
  dynamic get gridHeight;
  @override
  @JsonKey(name: 'gridWidth')
  dynamic get gridWidth;
  @override
  @JsonKey(name: 'offsetX')
  dynamic get offsetX;
  @override
  @JsonKey(name: 'offsetY')
  dynamic get offsetY;
  @override
  @JsonKey(ignore: true)
  _$$MosaicAlignmentValuesImplCopyWith<_$MosaicAlignmentValuesImpl>
      get copyWith => throw _privateConstructorUsedError;
}
