// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'channel_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChannelModel _$ChannelModelFromJson(Map<String, dynamic> json) => ChannelModel(
      status: json['status'] as String,
      data: (json['data'] as List<dynamic>?)
          ?.map((e) => Data.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ChannelModelToJson(ChannelModel instance) =>
    <String, dynamic>{
      'status': instance.status,
      'data': instance.data,
    };

_$DataImpl _$$DataImplFromJson(Map<String, dynamic> json) => _$DataImpl(
      id: json['id'] as int,
      uid: json['uid'] as String,
      position: json['position'] as int?,
      type: json['type'] as String?,
      resolution: json['resolution'] as String?,
      isMcast: json['is_mcast'] as bool?,
      isOtt: json['is_ott'] as bool?,
      isDvbt: json['is_dvbt'] as bool?,
      urlMcast: json['url_mcast'] as String?,
      urlOtt: json['url_ott'] as String?,
      recordable: json['recordable'] as bool?,
      recDuration: json['rec_duration'] as int?,
      timeshiftable: json['timeshiftable'] as bool?,
      tsRecDuration: json['ts_rec_duration'] as int?,
      parentalHidden: json['parental_hidden'] as bool?,
      dvbtTag: json['dvbt_tag'] as String?,
      streamPriority: json['stream_priority'] as String?,
      backgroundImageId: json['background_image_id'],
      metadata: json['metadata'] as String?,
      highlightsEnabled: json['highlights_enabled'] as bool?,
      ottType: json['ott_type'] as String?,
      name: json['name'] as String?,
      shortName: json['short_name'] as String?,
      epgChannel: json['epg_channel'] as String?,
      logos: json['logos'] == null
          ? null
          : Logos.fromJson(json['logos'] as Map<String, dynamic>),
      mosaicAlignmentValues: json['mosaic_alignment_values'] == null
          ? null
          : MosaicAlignmentValues.fromJson(
              json['mosaic_alignment_values'] as Map<String, dynamic>),
      cmChannel: json['cm_channel'] as String?,
    );

Map<String, dynamic> _$$DataImplToJson(_$DataImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'uid': instance.uid,
      'position': instance.position,
      'type': instance.type,
      'resolution': instance.resolution,
      'is_mcast': instance.isMcast,
      'is_ott': instance.isOtt,
      'is_dvbt': instance.isDvbt,
      'url_mcast': instance.urlMcast,
      'url_ott': instance.urlOtt,
      'recordable': instance.recordable,
      'rec_duration': instance.recDuration,
      'timeshiftable': instance.timeshiftable,
      'ts_rec_duration': instance.tsRecDuration,
      'parental_hidden': instance.parentalHidden,
      'dvbt_tag': instance.dvbtTag,
      'stream_priority': instance.streamPriority,
      'background_image_id': instance.backgroundImageId,
      'metadata': instance.metadata,
      'highlights_enabled': instance.highlightsEnabled,
      'ott_type': instance.ottType,
      'name': instance.name,
      'short_name': instance.shortName,
      'epg_channel': instance.epgChannel,
      'logos': instance.logos,
      'mosaic_alignment_values': instance.mosaicAlignmentValues,
      'cm_channel': instance.cmChannel,
    };

_$LogosImpl _$$LogosImplFromJson(Map<String, dynamic> json) => _$LogosImpl(
      card: json['CARD'],
      legacy: json['LEGACY'],
      normal: json['NORMAL'],
      shadow: json['SHADOW'],
    );

Map<String, dynamic> _$$LogosImplToJson(_$LogosImpl instance) =>
    <String, dynamic>{
      'CARD': instance.card,
      'LEGACY': instance.legacy,
      'NORMAL': instance.normal,
      'SHADOW': instance.shadow,
    };

_$MosaicAlignmentValuesImpl _$$MosaicAlignmentValuesImplFromJson(
        Map<String, dynamic> json) =>
    _$MosaicAlignmentValuesImpl(
      gridHeight: json['gridHeight'],
      gridWidth: json['gridWidth'],
      offsetX: json['offsetX'],
      offsetY: json['offsetY'],
    );

Map<String, dynamic> _$$MosaicAlignmentValuesImplToJson(
        _$MosaicAlignmentValuesImpl instance) =>
    <String, dynamic>{
      'gridHeight': instance.gridHeight,
      'gridWidth': instance.gridWidth,
      'offsetX': instance.offsetX,
      'offsetY': instance.offsetY,
    };
