import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:uniqcast_demo/services/http/api_exception.dart';

part 'result.freezed.dart';

@freezed
class Result<T> with _$Result<T> {
  const factory Result.initial() = ResultInitial;

  const factory Result.loading() = ResultLoading;

  const factory Result.success({required T data}) = ResultSuccess;

  const factory Result.failure(ApiException exception) = ResultFailure;
}
