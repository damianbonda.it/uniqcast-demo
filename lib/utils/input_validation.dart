import 'package:flutter/cupertino.dart';
import 'package:validators/validators.dart';
import 'package:emoji_regex/emoji_regex.dart';

class InputValidation {
  InputValidation({required this.context});

  final BuildContext context;

  final denyEmojisRegex = emojiRegex();

  String? emailValidator(String? value) {
    return value!.isNotEmpty && !isEmail(value)
        ? "Email is not valid..."
        : null;
  }

  String? passwordValidator(String? value) {
    final passwordRegExp = RegExp(r'^(?=.*[A-Za-z0-9])[A-Za-z\d]{2,}$');

    return value!.isNotEmpty && !passwordRegExp.hasMatch(value)
        ? "Password must contain 4 characters"
        : null;
  }

  String? nameValidator(String? value) {
    final nameValidator = RegExp(r'^(?! )[\w\s]{5,}$');

    return value!.isNotEmpty && !nameValidator.hasMatch(value)
        ? "Field cannot contain numbers and must not be empty"
        : null;
  }

  String? addressValidator(String? value) {
    final nameValidator = RegExp(r'^[#.0-9a-zA-Z\s,-]+$');

    return value!.isNotEmpty && !nameValidator.hasMatch(value)
        ? "Address must contain"
        : null;
  }
}
