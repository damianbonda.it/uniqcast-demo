import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:uniqcast_demo/services/authentication/authentication_service.dart';
import 'package:uniqcast_demo/services/authentication/authentication_service_impl.dart';
import 'package:uniqcast_demo/services/channel/channel_service.dart';
import 'package:uniqcast_demo/services/channel/channel_service_impl.dart';
import 'package:uniqcast_demo/services/package/package_service.dart';
import 'package:uniqcast_demo/services/package/package_service_impl.dart';

final authenticationRepositoryProvider = Provider<AuthenticationService>((ref) => AuthenticationServiceImpl());

final packageRepositoryProvider = Provider<PackageService>((ref) => PackageServiceImpl());

final channelRepositoryProvider = Provider<ChannelService>((ref) => ChannelServiceImpl());