# uniqcast_demo

This showcases a basic mobile video player built with Flutter, it uses generated Freezed models as well as a basic JWT authentication mechanism.

## Getting Started

Follow these steps to set up and run the Flutter video player app:

### Prerequisites

Ensure that you have Flutter installed on your machine. If not, follow the [Flutter installation guide](https://flutter.dev/docs/get-started/install).

### Run code generation

```bash
flutter pub get && flutter pub run build_runner build --delete-conflicting-outputs
```
### Run app

```bash
flutter run